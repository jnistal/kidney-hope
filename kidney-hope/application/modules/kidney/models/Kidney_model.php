<?php

use function GuzzleHttp\json_decode;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kidney_model extends CI_model
{

    /*****
     * KIDNEY_DONOR
     * id
     * patient_id
     * name
     * phone
     * bloodgroup
     * birthdate
     * sex
     */


    /***
     *TEST_INFO 
     *
     *patient_id 
     *donor_id
     *ant1
     *ant2
     *ant3
     *ant4
     *ant5
     *ant6
     *bloodgroup
     *weight
     *birthdate
     *sex
     */

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }





    /**
     * Obtiene los pacientes del doctor actual
     */
    function getPatientsByDoctor($doctor_id)
    {
        // $this->db->where('doctor_id',$doctor_id);
        $this->db->limit(20);
        $query = $this->db->get('patient');
        return $query->result();
    }

    /**
     * Obtiene los donantes del paciente con el id patient_id
     */
    function getDonors($patient_id)
    {
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get('kidney_donor');
        return $query->result();
    }

    /**
     * Obtiene el donante
     */
    function getDonor($donor_id)
    {
        $this->db->where('id', $donor_id);
        $query = $this->db->get('kidney_donor');
        return $query->row();
    }

    /**
     * Agrega un donador al paciente con id patient_id
     */
    function addDonor($patient_id, $donor)
    {
        $data1 = array('patient_id' => $patient_id);
        $data2 = array_merge($donor, $data1);
        $this->db->insert('kidney_donor', $data2);
        echo json_encode($this->db->error());
    }

    /**
     * Edita el donador
     */
    function editDonor($donor_id, $donor)
    {
        unset($donor['id']);
        unset($donor['patient_id']);
        $this->db->where('id', $donor_id);
        $this->db->update('kidney_donor', $donor);
        echo json_encode($this->db->error());
    }


    /**
     * Añade examenes del paciente (antigenos, peso, etc...)
     */
    function addPatientTest($patient_id, $test)
    {
        $data1 = array('patient_id' => $patient_id);
        $data2 = array_merge($test, $data1);

        //Buscar si test anterior existe
        $oldTest = $this->getPatientTest($patient_id);

        //Si ya existe, actualizarlo
        if (!empty($oldTest)) {
            $this->db->where('patient_id', $patient_id);
            $this->db->update('test_info',   $data2);
            return;
        }

        //Si no existe, insertarlo
        $this->db->insert('test_info',   $data2);
    }

    /**
     * Añade examenes del donador (antigenos, peso, etc...)
     */
    function addDonorTest($donor_id, $test)
    {
        $data1 = array('donor_id' => $donor_id);
        $data2 = array_merge($test, $data1);

        //Buscar si test anterior existe
        $oldTest = $this->getDonorTest($donor_id);

        //Si ya existe, actualizarlo
        if (!empty($oldTest)) {
            $this->db->where('donor_id', $donor_id);
            $this->db->update('test_info',   $data2);
            return;
        }

        //Si no existe, insertarlo
        $this->db->insert('test_info',   $data2);
    }


    /**
     * Obtiene la información médica del paciente
     */
    function getPatientTest($patient_id)
    {
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get('test_info');
        return $query->row();
    }

    /**
     * Obtiene la información médica del paciente
     */
    function getDonorTest($donor_id)
    {
        $this->db->where('donor_id', $donor_id);
        $query = $this->db->get('test_info');
        return $query->row();
    }
}