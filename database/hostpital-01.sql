-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.31 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para hospital
CREATE DATABASE IF NOT EXISTS `hospital` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hospital`;

-- Volcando estructura para tabla hospital.accountant
CREATE TABLE IF NOT EXISTS `accountant` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(200) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.accountant: 1 rows
/*!40000 ALTER TABLE `accountant` DISABLE KEYS */;
INSERT INTO `accountant` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `x`, `ion_user_id`, `hospital_id`) VALUES
	(84, NULL, 'Mr Accountant', 'accountant@hms.com', 'Collegepara, Rajbari', '+880123456789', NULL, '787', '466');
/*!40000 ALTER TABLE `accountant` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.alloted_bed
CREATE TABLE IF NOT EXISTS `alloted_bed` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `number` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `a_time` varchar(100) DEFAULT NULL,
  `d_time` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `bed_id` varchar(100) DEFAULT NULL,
  `patientname` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.alloted_bed: 2 rows
/*!40000 ALTER TABLE `alloted_bed` DISABLE KEYS */;
INSERT INTO `alloted_bed` (`id`, `number`, `category`, `patient`, `a_time`, `d_time`, `status`, `x`, `bed_id`, `patientname`, `hospital_id`) VALUES
	(48, NULL, NULL, '62', '21 December 2020 - 03:30 PM', '21 December 2020 - 03:35 PM', NULL, NULL, 'Hemodialisis-34', 'Julian Ramirez', '466'),
	(49, NULL, NULL, '62', '21 December 2020 - 03:30 PM', '21 December 2020 - 03:50 PM', NULL, NULL, 'Hemodialisis-34', 'Julian Ramirez', '466');
/*!40000 ALTER TABLE `alloted_bed` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.appointment
CREATE TABLE IF NOT EXISTS `appointment` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `time_slot` varchar(100) DEFAULT NULL,
  `s_time` varchar(100) DEFAULT NULL,
  `e_time` varchar(100) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `registration_time` varchar(100) DEFAULT NULL,
  `s_time_key` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `request` varchar(100) DEFAULT NULL,
  `patientname` varchar(1000) DEFAULT NULL,
  `doctorname` varchar(1000) DEFAULT NULL,
  `room_id` varchar(500) DEFAULT NULL,
  `live_meeting_link` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=466 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.appointment: 1 rows
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` (`id`, `patient`, `doctor`, `date`, `time_slot`, `s_time`, `e_time`, `remarks`, `add_date`, `registration_time`, `s_time_key`, `status`, `user`, `request`, `patientname`, `doctorname`, `room_id`, `live_meeting_link`, `hospital_id`) VALUES
	(465, '62', '162', '1603929600', '09:30 AM To 10:30 AM', '09:30 AM', '10:30 AM', '', '10/25/20', '1603640247', '114', 'Pending Confirmation', '763', '', 'Julian Ramirez', 'Dr. Carlos Dardon', 'hms-meeting-458948594-619905-466', 'https://meet.jit.si/hms-meeting-458948594-619905-466', '466');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.autoemailshortcode
CREATE TABLE IF NOT EXISTS `autoemailshortcode` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.autoemailshortcode: 32 rows
/*!40000 ALTER TABLE `autoemailshortcode` DISABLE KEYS */;
INSERT INTO `autoemailshortcode` (`id`, `name`, `type`) VALUES
	(1, '{firstname}', 'payment'),
	(2, '{lastname}', 'payment'),
	(3, '{name}', 'payment'),
	(4, '{amount}', 'payment'),
	(52, '{doctorname}', 'appoinment_confirmation'),
	(42, '{firstname}', 'appoinment_creation'),
	(51, '{name}', 'appoinment_confirmation'),
	(50, '{lastname}', 'appoinment_confirmation'),
	(49, '{firstname}', 'appoinment_confirmation'),
	(48, '{hospital_name}', 'appoinment_creation'),
	(47, '{time_slot}', 'appoinment_creation'),
	(46, '{appoinmentdate}', 'appoinment_creation'),
	(45, '{doctorname}', 'appoinment_creation'),
	(44, '{name}', 'appoinment_creation'),
	(43, '{lastname}', 'appoinment_creation'),
	(26, '{name}', 'doctor'),
	(27, '{firstname}', 'doctor'),
	(28, '{lastname}', 'doctor'),
	(29, '{company}', 'doctor'),
	(41, '{doctor}', 'patient'),
	(40, '{company}', 'patient'),
	(39, '{lastname}', 'patient'),
	(38, '{firstname}', 'patient'),
	(37, '{name}', 'patient'),
	(36, '{department}', 'doctor'),
	(53, '{appoinmentdate}', 'appoinment_confirmation'),
	(54, '{time_slot}', 'appoinment_confirmation'),
	(55, '{hospital_name}', 'appoinment_confirmation'),
	(56, '{start_time}', 'meeting_creation'),
	(57, '{patient_name}', 'meeting_creation'),
	(58, '{doctor_name}', 'meeting_creation'),
	(59, '{hospital_name}', 'meeting_creation');
/*!40000 ALTER TABLE `autoemailshortcode` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.autoemailtemplate
CREATE TABLE IF NOT EXISTS `autoemailtemplate` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.autoemailtemplate: 6 rows
/*!40000 ALTER TABLE `autoemailtemplate` DISABLE KEYS */;
INSERT INTO `autoemailtemplate` (`id`, `name`, `message`, `type`, `status`, `hospital_id`) VALUES
	(59, 'Patient Registration Confirmation', 'Dear {name}, You are registred to {company} as a patient to {doctor}. Regards', 'patient', 'Active', '466'),
	(58, 'Send Appointment confirmation to Doctor', 'Dear {name}, You are appointed as a doctor in {department} . Thank You {company}', 'doctor', 'Active', '466'),
	(57, 'Meeting Schedule Notification To Patient', 'Dear {patient_name}, You have a Live Video Meeting with {doctor_name} on {start_time}. For more information contact with {hospital_name} . Regards', 'meeting_creation', 'Active', '466'),
	(56, 'Appointment creation email to patient', 'Dear {name}, You have an appointment with {doctorname} on {appoinmentdate} at {time_slot} .Please confirm your appointment. For more information contact with {hospital_name} Regards', 'appoinment_creation', 'Active', '466'),
	(55, 'Appointment Confirmation email to patient', 'Dear {name}, Your appointment with {doctorname} on {appoinmentdate} at {time_slot} is confirmed. For more information contact with {hospital_name} Regards', 'appoinment_confirmation', 'Active', '466'),
	(54, 'Payment successful email to patient', 'Dear {name}, Your paying amount - Tk {amount} was successful. Thank You Please contact our support for further queries.', 'payment', 'Active', '466');
/*!40000 ALTER TABLE `autoemailtemplate` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.autosmsshortcode
CREATE TABLE IF NOT EXISTS `autosmsshortcode` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.autosmsshortcode: 32 rows
/*!40000 ALTER TABLE `autosmsshortcode` DISABLE KEYS */;
INSERT INTO `autosmsshortcode` (`id`, `name`, `type`) VALUES
	(1, '{name}', 'payment'),
	(2, '{firstname}', 'payment'),
	(3, '{lastname}', 'payment'),
	(4, '{amount}', 'payment'),
	(55, '{appoinmentdate}', 'appoinment_confirmation'),
	(54, '{doctorname}', 'appoinment_confirmation'),
	(53, '{name}', 'appoinment_confirmation'),
	(52, '{lastname}', 'appoinment_confirmation'),
	(51, '{firstname}', 'appoinment_confirmation'),
	(50, '{time_slot}', 'appoinment_creation'),
	(49, '{appoinmentdate}', 'appoinment_creation'),
	(48, '{hospital_name}', 'appoinment_creation'),
	(47, '{doctorname}', 'appoinment_creation'),
	(46, '{name}', 'appoinment_creation'),
	(45, '{lastname}', 'appoinment_creation'),
	(44, '{firstname}', 'appoinment_creation'),
	(28, '{firstname}', 'doctor'),
	(29, '{lastname}', 'doctor'),
	(30, '{name}', 'doctor'),
	(31, '{company}', 'doctor'),
	(43, '{doctor}', 'patient'),
	(42, '{company}', 'patient'),
	(41, '{lastname}', 'patient'),
	(40, '{firstname}', 'patient'),
	(39, '{name}', 'patient'),
	(38, '{department}', 'doctor'),
	(56, '{time_slot}', 'appoinment_confirmation'),
	(57, '{hospital_name}', 'appoinment_confirmation'),
	(58, '{start_time}', 'meeting_creation'),
	(59, '{patient_name}', 'meeting_creation'),
	(60, '{doctor_name}', 'meeting_creation'),
	(61, '{hospital_name}', 'meeting_creation');
/*!40000 ALTER TABLE `autosmsshortcode` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.autosmstemplate
CREATE TABLE IF NOT EXISTS `autosmstemplate` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.autosmstemplate: 6 rows
/*!40000 ALTER TABLE `autosmstemplate` DISABLE KEYS */;
INSERT INTO `autosmstemplate` (`id`, `name`, `message`, `type`, `status`, `hospital_id`) VALUES
	(69, 'Patient Registration Confirmation', 'Dear {name}, You are registred to {company} as a patient to {doctor}. Regards', 'patient', 'Active', '466'),
	(68, 'send appoint confirmation to Doctor', 'Dear {name}, You are appointed as a doctor in {department} . Thank You {company}', 'doctor', 'Active', '466'),
	(67, 'Meeting Schedule Notification To Patient', 'Dear {patient_name}, You have a Live Video Meeting with {doctor_name} on {start_time}. For more information contact with {hospital_name} . Regards', 'meeting_creation', 'Active', '466'),
	(66, 'Appointment creation sms to patient', 'Dear {name}, You have an appointment with {doctorname} on {appoinmentdate} at {time_slot} .Please confirm your appointment. For more information contact with {hospital_name} Regards', 'appoinment_creation', 'Active', '466'),
	(65, 'Appointment Confirmation sms to patient', 'Dear {name}, Your appointment with {doctorname} on {appoinmentdate} at {time_slot} is confirmed. For more information contact with {hospital_name} Regards', 'appoinment_confirmation', 'Active', '466'),
	(64, 'Payment successful sms to patient', 'Dear {name}, Your paying amount - Tk {amount} was successful. Thank You Please contact our support for further queries.', 'payment', 'Active', '466');
/*!40000 ALTER TABLE `autosmstemplate` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.bankb
CREATE TABLE IF NOT EXISTS `bankb` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `group` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.bankb: 8 rows
/*!40000 ALTER TABLE `bankb` DISABLE KEYS */;
INSERT INTO `bankb` (`id`, `group`, `status`, `hospital_id`) VALUES
	(72, 'O-', '0 Bags', '466'),
	(71, 'O+', '0 Bags', '466'),
	(70, 'AB-', '0 Bags', '466'),
	(69, 'AB+', '0 Bags', '466'),
	(68, 'B-', '0 Bags', '466'),
	(67, 'B+', '0 Bags', '466'),
	(66, 'A-', '0 Bags', '466'),
	(65, 'A+', '0 Bags', '466');
/*!40000 ALTER TABLE `bankb` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.bed
CREATE TABLE IF NOT EXISTS `bed` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `number` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `last_a_time` varchar(100) DEFAULT NULL,
  `last_d_time` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `bed_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.bed: 1 rows
/*!40000 ALTER TABLE `bed` DISABLE KEYS */;
INSERT INTO `bed` (`id`, `category`, `number`, `description`, `last_a_time`, `last_d_time`, `status`, `bed_id`, `hospital_id`) VALUES
	(22, 'Hemodialisis', '34', 'Equipo #34', '21 December 2020 - 03:30 PM', '21 December 2020 - 03:50 PM', NULL, 'Hemodialisis-34', '466');
/*!40000 ALTER TABLE `bed` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.bed_category
CREATE TABLE IF NOT EXISTS `bed_category` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.bed_category: 1 rows
/*!40000 ALTER TABLE `bed_category` DISABLE KEYS */;
INSERT INTO `bed_category` (`id`, `category`, `description`, `hospital_id`) VALUES
	(15, 'Hemodialisis', 'Hemodialisis', '466');
/*!40000 ALTER TABLE `bed_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.department
CREATE TABLE IF NOT EXISTS `department` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `x` varchar(10) DEFAULT NULL,
  `y` varchar(10) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.department: 1 rows
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`id`, `name`, `description`, `x`, `y`, `hospital_id`) VALUES
	(58, 'Cardiology', '<p>Description</p>\r\n', NULL, NULL, '466');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.diagnostic_report
CREATE TABLE IF NOT EXISTS `diagnostic_report` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `invoice` varchar(100) DEFAULT NULL,
  `report` varchar(10000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.diagnostic_report: 0 rows
/*!40000 ALTER TABLE `diagnostic_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `diagnostic_report` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.doctor
CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `profile` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(10) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.doctor: 1 rows
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `department`, `profile`, `x`, `y`, `ion_user_id`, `hospital_id`) VALUES
	(162, 'uploads/fundanier.png', 'Dr. Carlos Dardon', 'doctor@hms.com', 'Ciudad de guatemala', '48594589', 'Cardiology', 'Médico General', NULL, NULL, '765', '466');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.donor
CREATE TABLE IF NOT EXISTS `donor` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `group` varchar(10) DEFAULT NULL,
  `age` varchar(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `ldd` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.donor: 0 rows
/*!40000 ALTER TABLE `donor` DISABLE KEYS */;
/*!40000 ALTER TABLE `donor` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.email
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `message` varchar(10000) DEFAULT NULL,
  `reciepient` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.email: 0 rows
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.email_settings
CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.email_settings: 1 rows
/*!40000 ALTER TABLE `email_settings` DISABLE KEYS */;
INSERT INTO `email_settings` (`id`, `admin_email`, `type`, `user`, `password`, `hospital_id`) VALUES
	(13, 'admin@codearistos.net', NULL, NULL, NULL, '466');
/*!40000 ALTER TABLE `email_settings` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.expense
CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `datestring` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.expense: 0 rows
/*!40000 ALTER TABLE `expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.expense_category
CREATE TABLE IF NOT EXISTS `expense_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.expense_category: 0 rows
/*!40000 ALTER TABLE `expense_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.featured
CREATE TABLE IF NOT EXISTS `featured` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(1000) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `profile` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.featured: 4 rows
/*!40000 ALTER TABLE `featured` DISABLE KEYS */;
INSERT INTO `featured` (`id`, `img_url`, `name`, `profile`, `description`) VALUES
	(1, 'uploads/images.jpg', 'Dr Momenuzzaman', 'Cardiac Specialized', 'Redantium, totam rem aperiam, eaque ipsa qu ab illo inventore veritatis et quasi architectos beatae vitae dicta sunt explicabo. Nemo enims sadips ipsums un.'),
	(2, 'uploads/doctor.png', 'Dr RahmatUllah Asif', 'Cardiac Specialized', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(3, 'uploads/download_(2)2.png', 'Dr A.R.M. Jamil', 'Cardiac Specialized', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(4, 'uploads/inlinePreview.jpg', 'Hospital Management Syatem', 'Cardiac Specialized', '<p>bfbjfbsjbjsbfjsbf</p>\r\n');
/*!40000 ALTER TABLE `featured` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.groups: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1, 'superadmin', 'Super Admin'),
	(2, 'members', 'General User'),
	(3, 'Accountant', 'For Financial Activities'),
	(4, 'Doctor', ''),
	(5, 'Patient', ''),
	(6, 'Nurse', ''),
	(7, 'Pharmacist', ''),
	(8, 'Laboratorist', ''),
	(10, 'Receptionist', 'Receptionist'),
	(11, 'admin', 'Administrator');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.holidays
CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.holidays: 0 rows
/*!40000 ALTER TABLE `holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `holidays` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.hospital
CREATE TABLE IF NOT EXISTS `hospital` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL,
  `package` varchar(100) DEFAULT NULL,
  `p_limit` varchar(100) DEFAULT NULL,
  `d_limit` varchar(100) DEFAULT NULL,
  `module` varchar(1000) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=477 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.hospital: 1 rows
/*!40000 ALTER TABLE `hospital` DISABLE KEYS */;
INSERT INTO `hospital` (`id`, `name`, `email`, `password`, `address`, `phone`, `package`, `p_limit`, `d_limit`, `module`, `ion_user_id`) VALUES
	(466, 'Fundanier', 'admin@hms.com', NULL, 'Ciudad de Guatemala', '+502 30006780', '', '1000', '500', 'accountant,appointment,lab,bed,department,doctor,donor,finance,pharmacy,laboratorist,medicine,nurse,patient,pharmacist,prescription,receptionist,report,notice,email,sms,kidney', '763');
/*!40000 ALTER TABLE `hospital` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.kidney_donor
CREATE TABLE IF NOT EXISTS `kidney_donor` (
  `id` varchar(11) NOT NULL,
  `patient_id` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `birthdate` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `bloodgroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla hospital.kidney_donor: 6 rows
/*!40000 ALTER TABLE `kidney_donor` DISABLE KEYS */;
INSERT INTO `kidney_donor` (`id`, `patient_id`, `name`, `address`, `phone`, `birthdate`, `sex`, `bloodgroup`) VALUES
	('938994', '646905', 'Donante 1', 'Chiquimula', '55681256', '10-02-2021', 'Male', 'O-'),
	('45008', '646905', 'Donador 2', 'Chiquimula', '55681256', '16-02-2021', 'Male', 'O-'),
	('577685', '59300', 'Nuevo Donador', 'Chiquimula', '55681256', '16-02-2021', 'Male', 'AB-'),
	('628950', '256271', 'de nuevo', 'Chiquimula', '55681256', '16-02-2021', 'Male', 'O-'),
	('258242', '59300', 'Donador', 'Chiquimula', '55681256', '17-02-2021', 'Male', 'O-'),
	('971836', '158098', 'FSAD', 'DSFAS', '1FSAD', '18-02-2021', 'Male', 'AB-');
/*!40000 ALTER TABLE `kidney_donor` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.lab
CREATE TABLE IF NOT EXISTS `lab` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `category_name` varchar(1000) DEFAULT NULL,
  `report` varchar(10000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `patient_name` varchar(100) DEFAULT NULL,
  `patient_phone` varchar(100) DEFAULT NULL,
  `patient_address` varchar(100) DEFAULT NULL,
  `doctor_name` varchar(100) DEFAULT NULL,
  `date_string` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1936 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.lab: 0 rows
/*!40000 ALTER TABLE `lab` DISABLE KEYS */;
/*!40000 ALTER TABLE `lab` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.laboratorist
CREATE TABLE IF NOT EXISTS `laboratorist` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.laboratorist: 1 rows
/*!40000 ALTER TABLE `laboratorist` DISABLE KEYS */;
INSERT INTO `laboratorist` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `x`, `y`, `ion_user_id`, `hospital_id`) VALUES
	(6, NULL, 'Mr Laboratorist', 'laboratorist@hms.com', 'Collegepara, Rajbari', '+880123456789', NULL, NULL, '789', '466');
/*!40000 ALTER TABLE `laboratorist` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.lab_category
CREATE TABLE IF NOT EXISTS `lab_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `reference_value` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.lab_category: 47 rows
/*!40000 ALTER TABLE `lab_category` DISABLE KEYS */;
INSERT INTO `lab_category` (`id`, `category`, `description`, `reference_value`, `hospital_id`) VALUES
	(35, 'Troponin-I', 'Pathological Test', '', NULL),
	(36, 'CBC (DIGITAL)', 'Pathological Test', '', NULL),
	(37, 'Eosinophil', 'Pathological Test', '', NULL),
	(38, 'Platelets', 'Pathological Test', '', NULL),
	(39, 'Malarial Parasites (MP)', 'Pathological Test', '', NULL),
	(40, 'BT/ CT', 'Pathological Test', '', NULL),
	(41, 'ASO Titre', 'Pathological Test', '', NULL),
	(42, 'CRP', 'Pathological Test', '', NULL),
	(43, 'R/A test', 'Pathological Test', '', NULL),
	(44, 'VDRL', 'Pathological Test', '', NULL),
	(45, 'TPHA', 'Pathological Test', '', NULL),
	(46, 'HBsAg (Screening)', 'Pathological Test', '', NULL),
	(47, 'HBsAg (Confirmatory)', 'Pathological Test', '', NULL),
	(48, 'CFT for Kala Zar', 'Pathological Test', '', NULL),
	(49, 'CFT for Filaria', 'Pathological Test', '', NULL),
	(50, 'Pregnancy Test', 'Pathological Test', '', NULL),
	(51, 'Blood Grouping', 'Pathological Test', '', NULL),
	(52, 'Widal Test', 'Pathological Test', '(70-110)mg/dl', NULL),
	(53, 'RBS', 'Pathological Test', '', NULL),
	(54, 'Blood Urea', 'Pathological Test', '', NULL),
	(55, 'S. Creatinine', 'Pathological Test', '', NULL),
	(56, 'S. cholesterol', 'Pathological Test', '', NULL),
	(57, 'Fasting Lipid Profile', 'Pathological Test', '', NULL),
	(58, 'S. Bilirubin', 'Pathological Test', '', NULL),
	(59, 'S. Alkaline Phosohare', 'Pathological Test', '', NULL),
	(61, 'S. Calcium', 'Pathological Test', '', NULL),
	(62, 'RBS with CUS', 'Pathological Test', '', NULL),
	(63, 'SGPT', 'Pathological Test', '', NULL),
	(64, 'SGOT', 'Pathological Test', '', NULL),
	(65, 'Urine for R/E', 'Pathological Test', '', NULL),
	(66, 'Urine C/S', 'Pathological Test', '', NULL),
	(67, 'Stool for R/E', 'Pathological Test', '', NULL),
	(68, 'Semen Analysis', 'Pathological Test', '', NULL),
	(69, 'S. Electrolyte', 'Pathological Test', '', NULL),
	(70, 'S. T3/ T4/ THS', 'Pathological Test', '', NULL),
	(71, 'MT', 'Pathological Test', '', NULL),
	(106, 'ESR', 'Patho Test', '', NULL),
	(107, 'FBS CUS', 'Pathological test', '', NULL),
	(108, 'Hb%', 'Pathological test', '', NULL),
	(114, '2HABF', 'Pathological test', '', NULL),
	(113, 'FBS', 'Pathological test', '', NULL),
	(115, 'S. TSH', 'Pathological test', '', NULL),
	(116, 'S. T3', 'Pathological test', '', NULL),
	(117, 'DC', 'Pathological test', '', NULL),
	(118, 'TC', 'Pathological test', '', NULL),
	(120, 'S. Uric acid', 'Pathological test', '', NULL),
	(126, 'eosinphil', 'Pathology Test', '', NULL);
/*!40000 ALTER TABLE `lab_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.login_attempts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.manualemailshortcode
CREATE TABLE IF NOT EXISTS `manualemailshortcode` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.manualemailshortcode: 7 rows
/*!40000 ALTER TABLE `manualemailshortcode` DISABLE KEYS */;
INSERT INTO `manualemailshortcode` (`id`, `name`, `type`) VALUES
	(1, '{firstname}', 'email'),
	(2, '{lastname}', 'email'),
	(3, '{name}', 'email'),
	(6, '{address}', 'email'),
	(7, '{company}', 'email'),
	(8, '{email}', 'email'),
	(9, '{phone}', 'email');
/*!40000 ALTER TABLE `manualemailshortcode` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.manualsmsshortcode
CREATE TABLE IF NOT EXISTS `manualsmsshortcode` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.manualsmsshortcode: 7 rows
/*!40000 ALTER TABLE `manualsmsshortcode` DISABLE KEYS */;
INSERT INTO `manualsmsshortcode` (`id`, `name`, `type`) VALUES
	(1, '{firstname}', 'sms'),
	(2, '{lastname}', 'sms'),
	(3, '{name}', 'sms'),
	(4, '{email}', 'sms'),
	(5, '{phone}', 'sms'),
	(6, '{address}', 'sms'),
	(10, '{company}', 'sms');
/*!40000 ALTER TABLE `manualsmsshortcode` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.manual_email_template
CREATE TABLE IF NOT EXISTS `manual_email_template` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.manual_email_template: 1 rows
/*!40000 ALTER TABLE `manual_email_template` DISABLE KEYS */;
INSERT INTO `manual_email_template` (`id`, `name`, `message`, `type`, `hospital_id`) VALUES
	(11, 'Template', '{phone} {email}', 'email', '466');
/*!40000 ALTER TABLE `manual_email_template` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.manual_sms_template
CREATE TABLE IF NOT EXISTS `manual_sms_template` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.manual_sms_template: 0 rows
/*!40000 ALTER TABLE `manual_sms_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `manual_sms_template` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.medical_history
CREATE TABLE IF NOT EXISTS `medical_history` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `patient_name` varchar(100) DEFAULT NULL,
  `patient_address` varchar(500) DEFAULT NULL,
  `patient_phone` varchar(100) DEFAULT NULL,
  `img_url` varchar(500) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `registration_time` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.medical_history: 0 rows
/*!40000 ALTER TABLE `medical_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `medical_history` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.medicine
CREATE TABLE IF NOT EXISTS `medicine` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `box` varchar(100) DEFAULT NULL,
  `s_price` varchar(100) DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL,
  `generic` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `effects` varchar(100) DEFAULT NULL,
  `e_date` varchar(70) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2879 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.medicine: 1 rows
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` (`id`, `name`, `category`, `price`, `box`, `s_price`, `quantity`, `generic`, `company`, `effects`, `e_date`, `add_date`, `hospital_id`) VALUES
	(2878, 'NEUROBion', 'Neurologico', '7', 'Lozapren', '20', 1050, 'ezomeprazol', 'Edificio Atrium', '', '22-10-2020', '10/23/20', '466');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.medicine_category
CREATE TABLE IF NOT EXISTS `medicine_category` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.medicine_category: 1 rows
/*!40000 ALTER TABLE `medicine_category` DISABLE KEYS */;
INSERT INTO `medicine_category` (`id`, `category`, `description`, `hospital_id`) VALUES
	(26, 'Neurologico', 'Neurologico', '466');
/*!40000 ALTER TABLE `medicine_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.meeting
CREATE TABLE IF NOT EXISTS `meeting` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `meeting_id` varchar(100) DEFAULT NULL,
  `meeting_password` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `time_slot` varchar(100) DEFAULT NULL,
  `s_time` varchar(100) DEFAULT NULL,
  `e_time` varchar(100) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `registration_time` varchar(100) DEFAULT NULL,
  `s_time_key` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `request` varchar(100) DEFAULT NULL,
  `patientname` varchar(1000) DEFAULT NULL,
  `doctorname` varchar(1000) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `doctor_ion_id` varchar(100) DEFAULT NULL,
  `patient_ion_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=588 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.meeting: 0 rows
/*!40000 ALTER TABLE `meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `meeting` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.meeting_settings
CREATE TABLE IF NOT EXISTS `meeting_settings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(100) DEFAULT NULL,
  `secret_key` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.meeting_settings: 0 rows
/*!40000 ALTER TABLE `meeting_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meeting_settings` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `hospital_id` varchar(100) DEFAULT NULL,
  `modules` varchar(1000) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.module: 0 rows
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.notice
CREATE TABLE IF NOT EXISTS `notice` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.notice: 0 rows
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.nurse
CREATE TABLE IF NOT EXISTS `nurse` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `z` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.nurse: 1 rows
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `x`, `y`, `z`, `ion_user_id`, `hospital_id`) VALUES
	(17, NULL, 'Mrs Nurse', 'nurse@hms.com', 'Collegepara, Rajbari', '+880123456789', NULL, NULL, NULL, '790', '466');
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.ot_payment
CREATE TABLE IF NOT EXISTS `ot_payment` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `patient` varchar(100) DEFAULT NULL,
  `doctor_c_s` varchar(100) DEFAULT NULL,
  `doctor_a_s_1` varchar(100) DEFAULT NULL,
  `doctor_a_s_2` varchar(100) DEFAULT NULL,
  `doctor_anaes` varchar(100) DEFAULT NULL,
  `n_o_o` varchar(100) DEFAULT NULL,
  `c_s_f` varchar(100) DEFAULT NULL,
  `a_s_f_1` varchar(100) DEFAULT NULL,
  `a_s_f_2` varchar(11) DEFAULT NULL,
  `anaes_f` varchar(100) DEFAULT NULL,
  `ot_charge` varchar(100) DEFAULT NULL,
  `cab_rent` varchar(100) DEFAULT NULL,
  `seat_rent` varchar(100) DEFAULT NULL,
  `others` varchar(100) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `doctor_fees` varchar(100) DEFAULT NULL,
  `hospital_fees` varchar(100) DEFAULT NULL,
  `gross_total` varchar(100) DEFAULT NULL,
  `flat_discount` varchar(100) DEFAULT NULL,
  `amount_received` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.ot_payment: 1 rows
/*!40000 ALTER TABLE `ot_payment` DISABLE KEYS */;
INSERT INTO `ot_payment` (`id`, `patient`, `doctor_c_s`, `doctor_a_s_1`, `doctor_a_s_2`, `doctor_anaes`, `n_o_o`, `c_s_f`, `a_s_f_1`, `a_s_f_2`, `anaes_f`, `ot_charge`, `cab_rent`, `seat_rent`, `others`, `discount`, `date`, `amount`, `doctor_fees`, `hospital_fees`, `gross_total`, `flat_discount`, `amount_received`, `status`, `user`, `hospital_id`) VALUES
	(85, '451', 'None', '123', 'None', '125', 'dbdbd', '', '1000', '0', '1000', '', '', '', '', '', '1506195494', '2000', '2000', '0', '2000', '', '1000', 'unpaid', '614', NULL);
/*!40000 ALTER TABLE `ot_payment` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.package
CREATE TABLE IF NOT EXISTS `package` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `p_limit` varchar(100) DEFAULT NULL,
  `d_limit` varchar(100) DEFAULT NULL,
  `module` varchar(1000) DEFAULT NULL,
  `show_in_frontend` varchar(100) DEFAULT NULL,
  `frontend_order` varchar(100) DEFAULT NULL,
  `set_as_default` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.package: 1 rows
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` (`id`, `name`, `price`, `p_limit`, `d_limit`, `module`, `show_in_frontend`, `frontend_order`, `set_as_default`) VALUES
	(80, 'asdasd', '3000', '2500', '1000', 'accountant,appointment,lab,bed,department,doctor,donor,finance,pharmacy,laboratorist,medicine,nurse,patient,pharmacist,prescription,receptionist,report,notice,email,sms,kidney', 'Yes', NULL, NULL);
/*!40000 ALTER TABLE `package` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.patient
CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `birthdate` varchar(100) DEFAULT NULL,
  `age` varchar(100) DEFAULT NULL,
  `bloodgroup` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `patient_id` varchar(100) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `registration_time` varchar(100) DEFAULT NULL,
  `how_added` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.patient: 13 rows
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` (`id`, `img_url`, `name`, `email`, `doctor`, `address`, `phone`, `sex`, `birthdate`, `age`, `bloodgroup`, `ion_user_id`, `patient_id`, `add_date`, `registration_time`, `how_added`, `hospital_id`) VALUES
	(62, 'uploads/photos_0021.JPG', 'Julian Ramirez', 'patient@hms.com', ',162', 'Ciudad de Guatemala', '458948594', 'Male', '01-07-2020', NULL, 'O-', '764', '158098', '07/28/20', '1595924679', NULL, '466'),
	(66, NULL, 'Juan Jose', 'a@juango.com', NULL, 'Dirección', '3434543', 'Male', '22-12-2020', NULL, 'A+', '791', '59300', '12/22/20', '1608657463', NULL, '466'),
	(67, NULL, 'josguerra99@gmail.com', 'josguerra99@gmail.com', NULL, 'Chiquimula', '55681256', 'Male', '01-12-1999', NULL, 'O+', '792', '314383', '12/29/20', '1609280481', NULL, '466'),
	(68, 'uploads/528682868309486372409377743484193945944064N.jpg', 'José Guerra', 'aaaa@hms.com', '162', 'Chiquimula', '55681256', 'Male', '01-12-2000', NULL, 'O-', '793', '214218', '12/29/20', '1609280639', NULL, '466'),
	(69, NULL, 'Paciente Editado', 'nuevo@hms.com', '162', 'Chiquimula', '55681256', 'Male', '01-01-1999', NULL, 'O-', '794', '683155', '12/29/20', '1609282461', NULL, '466'),
	(70, NULL, 'eeeeeeeeee', 'test1@hms.com', '162', 'Chiquimula', '55681256', 'Male', '01-01-1999', NULL, 'O-', '795', '22558', '12/31/20', '1609377644', NULL, '466'),
	(76, 'uploads/51y6AWXREiL_SS5002.jpg', 'José Guerra', 'jnfas@fsdaf', '162', 'Direccion', '55681256', 'Male', '07-01-2021', NULL, 'B-', '801', '646905', '01/08/21', '1610068579', NULL, '466'),
	(75, 'uploads/51y6AWXREiL_SS500.jpg', 'aaaa', 'elcorreo@nuevo', '162', 'lo mas nuevo', '98765432', 'Male', '07-01-2021', NULL, 'AB-', '800', '', '01/07/21', '1610005153', NULL, '466'),
	(77, NULL, 'EDITADOOO', 'fsdafs@sgfasd', '162', 'Chiquimula', '55681256', 'Male', '01-01-1999', NULL, 'O-', '802', '118800', '01/15/21', '1610674126', NULL, '466'),
	(78, NULL, 'Paciente Nombre', 'a@aaaa', '162', 'Direccion', '12345678', 'Male', '29-01-2021', NULL, 'O-', '803', '256271', '01/22/21', '1611357859', NULL, '466'),
	(79, NULL, 'Paciente Nombre', 'a@aaaab', '162', 'Direccion', '12345678', 'Male', '29-01-2021', NULL, 'O-', '804', '396728', '01/22/21', '1611357937', NULL, '466'),
	(80, NULL, 'aaaa', 'admidfsadn@hms.com', '162', 'addafd', 'aa', 'Female', '15-01-2021', NULL, 'O-', '805', '916775', '01/22/21', '1611358154', NULL, '466'),
	(81, NULL, 'bb', 'b@bbb', '162', 'bbbbb', '55681256', 'Male', '06-01-2021', NULL, 'O-', '806', '590863', '01/23/21', '1611360705', NULL, '466');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.patient_deposit
CREATE TABLE IF NOT EXISTS `patient_deposit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `patient` varchar(100) DEFAULT NULL,
  `payment_id` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `deposited_amount` varchar(100) DEFAULT NULL,
  `amount_received_id` varchar(100) DEFAULT NULL,
  `deposit_type` varchar(100) DEFAULT NULL,
  `gateway` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1661 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.patient_deposit: 0 rows
/*!40000 ALTER TABLE `patient_deposit` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_deposit` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.patient_material
CREATE TABLE IF NOT EXISTS `patient_material` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `patient_name` varchar(100) DEFAULT NULL,
  `patient_address` varchar(100) DEFAULT NULL,
  `patient_phone` varchar(100) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `date_string` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.patient_material: 0 rows
/*!40000 ALTER TABLE `patient_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_material` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.patient_parents
CREATE TABLE IF NOT EXISTS `patient_parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `dpi` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `birthdate` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla hospital.patient_parents: 7 rows
/*!40000 ALTER TABLE `patient_parents` DISABLE KEYS */;
INSERT INTO `patient_parents` (`id`, `patient_id`, `name`, `sex`, `dpi`, `address`, `phone`, `birthdate`, `email`) VALUES
	(7, 214218, 'José Guerra', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-01-1999', 'josguerra99@gmail.com'),
	(2, 683155, 'Editado', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-12-2000', 'nuevoemail@gmail.com'),
	(3, 22558, 'José Guerra', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-12-2000', 'josguerra99@gmail.com'),
	(4, NULL, 'José Guerra', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-01-1999', 'josguerra99@gmail.com'),
	(9, 646905, 'José Guerra', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-01-1999', 'josguerra99@gmail.com'),
	(10, 646905, 'Ticket 2', 'Male', '1234567891234', 'Chiquimula', '12345678', '08-01-2021', 'ticket@gmail.com'),
	(11, 118800, 'José Guerra', 'Male', '1234567891234', 'Chiquimula', '55681256', '01-01-1999', 'josguerra99@gmail.com');
/*!40000 ALTER TABLE `patient_parents` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `vat` varchar(100) NOT NULL DEFAULT '0',
  `x_ray` varchar(100) DEFAULT NULL,
  `flat_vat` varchar(100) DEFAULT NULL,
  `discount` varchar(100) NOT NULL DEFAULT '0',
  `flat_discount` varchar(100) DEFAULT NULL,
  `gross_total` varchar(100) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `hospital_amount` varchar(100) DEFAULT NULL,
  `doctor_amount` varchar(100) DEFAULT NULL,
  `category_amount` varchar(1000) DEFAULT NULL,
  `category_name` varchar(1000) DEFAULT NULL,
  `amount_received` varchar(100) DEFAULT NULL,
  `deposit_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `patient_name` varchar(100) DEFAULT NULL,
  `patient_phone` varchar(100) DEFAULT NULL,
  `patient_address` varchar(100) DEFAULT NULL,
  `doctor_name` varchar(100) DEFAULT NULL,
  `date_string` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2077 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.payment: 0 rows
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.paymentgateway
CREATE TABLE IF NOT EXISTS `paymentgateway` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `merchant_key` varchar(100) DEFAULT NULL,
  `salt` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `APIUsername` varchar(100) DEFAULT NULL,
  `APIPassword` varchar(100) DEFAULT NULL,
  `APISignature` varchar(100) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `publish` varchar(1000) DEFAULT NULL,
  `secret` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  `public_key` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.paymentgateway: 4 rows
/*!40000 ALTER TABLE `paymentgateway` DISABLE KEYS */;
INSERT INTO `paymentgateway` (`id`, `name`, `merchant_key`, `salt`, `x`, `y`, `APIUsername`, `APIPassword`, `APISignature`, `status`, `publish`, `secret`, `hospital_id`, `public_key`) VALUES
	(27, 'Stripe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', 'Enter Publish key', 'Enter Secret Key', '466', NULL),
	(26, 'Pay U Money', 'Enter Merchant key', 'Enter Salt', NULL, NULL, NULL, NULL, NULL, 'test', NULL, NULL, '466', NULL),
	(25, 'PayPal', NULL, NULL, NULL, NULL, 'Enter API Username', 'Enter API Password', 'Enter API Signature', 'test', NULL, NULL, '466', NULL),
	(31, 'Paystack', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, 'Enter Secret Key', '466', 'Enter Public Key');
/*!40000 ALTER TABLE `paymentgateway` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.payment_category
CREATE TABLE IF NOT EXISTS `payment_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `c_price` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `d_commission` int(100) DEFAULT NULL,
  `h_commission` int(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.payment_category: 0 rows
/*!40000 ALTER TABLE `payment_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.pharmacist
CREATE TABLE IF NOT EXISTS `pharmacist` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.pharmacist: 1 rows
/*!40000 ALTER TABLE `pharmacist` DISABLE KEYS */;
INSERT INTO `pharmacist` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `x`, `y`, `ion_user_id`, `hospital_id`) VALUES
	(10, NULL, 'Mr Pharmacist', 'pharmacist@hms.com', 'Collegepara, Rajbari', '+880123456789', NULL, NULL, '767', '466');
/*!40000 ALTER TABLE `pharmacist` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.pharmacy_expense
CREATE TABLE IF NOT EXISTS `pharmacy_expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.pharmacy_expense: 0 rows
/*!40000 ALTER TABLE `pharmacy_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `pharmacy_expense` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.pharmacy_expense_category
CREATE TABLE IF NOT EXISTS `pharmacy_expense_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `y` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.pharmacy_expense_category: 0 rows
/*!40000 ALTER TABLE `pharmacy_expense_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `pharmacy_expense_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.pharmacy_payment
CREATE TABLE IF NOT EXISTS `pharmacy_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `vat` varchar(100) NOT NULL DEFAULT '0',
  `x_ray` varchar(100) DEFAULT NULL,
  `flat_vat` varchar(100) DEFAULT NULL,
  `discount` varchar(100) NOT NULL DEFAULT '0',
  `flat_discount` varchar(100) DEFAULT NULL,
  `gross_total` varchar(100) DEFAULT NULL,
  `hospital_amount` varchar(100) DEFAULT NULL,
  `doctor_amount` varchar(100) DEFAULT NULL,
  `category_amount` varchar(1000) DEFAULT NULL,
  `category_name` varchar(1000) DEFAULT NULL,
  `amount_received` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1981 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.pharmacy_payment: 0 rows
/*!40000 ALTER TABLE `pharmacy_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pharmacy_payment` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.pharmacy_payment_category
CREATE TABLE IF NOT EXISTS `pharmacy_payment_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `c_price` varchar(100) DEFAULT NULL,
  `d_commission` int(100) DEFAULT NULL,
  `h_commission` int(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.pharmacy_payment_category: 0 rows
/*!40000 ALTER TABLE `pharmacy_payment_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `pharmacy_payment_category` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.prescription
CREATE TABLE IF NOT EXISTS `prescription` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `symptom` varchar(100) DEFAULT NULL,
  `advice` varchar(1000) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `dd` varchar(100) DEFAULT NULL,
  `medicine` varchar(1000) DEFAULT NULL,
  `validity` varchar(100) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `patientname` varchar(1000) DEFAULT NULL,
  `doctorname` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.prescription: 1 rows
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` (`id`, `date`, `patient`, `doctor`, `symptom`, `advice`, `state`, `dd`, `medicine`, `validity`, `note`, `patientname`, `doctorname`, `hospital_id`) VALUES
	(101, '1608508800', '62', '162', '<p>Dificultad para dormir&nbsp;</p>\r\n', '<p>De sentir los sintomas evitar dormir tarde&nbsp;</p>\r\n', NULL, NULL, '2878***50 g***1***7 dias***Antes de comer', NULL, '<p>Reducir dosis de supresor</p>\r\n', 'Julian Ramirez', 'Dr. Carlos Dardon', '466');
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.receptionist
CREATE TABLE IF NOT EXISTS `receptionist` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `ion_user_id` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.receptionist: 1 rows
/*!40000 ALTER TABLE `receptionist` DISABLE KEYS */;
INSERT INTO `receptionist` (`id`, `img_url`, `name`, `email`, `address`, `phone`, `x`, `ion_user_id`, `hospital_id`) VALUES
	(9, NULL, 'Mr Receptionist', 'receptionist@hms.com', 'Collegepara, Rajbari', '+880123456789', NULL, '770', '466');
/*!40000 ALTER TABLE `receptionist` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.report
CREATE TABLE IF NOT EXISTS `report` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `report_type` varchar(100) DEFAULT NULL,
  `patient` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `add_date` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.report: 0 rows
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.request
CREATE TABLE IF NOT EXISTS `request` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `other` varchar(100) DEFAULT NULL,
  `package` varchar(1000) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.request: 2 rows
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
INSERT INTO `request` (`id`, `name`, `address`, `email`, `phone`, `other`, `package`, `language`, `remarks`, `status`) VALUES
	(18, 'asdasdasd', 'asdasdasdas', 'wewerwe@gmail.com', '2423423423', NULL, '80', 'english', NULL, 'Approved'),
	(19, 'czdsffsdfs', 'sdfsdfefsd', 'sdfdfcfdsf@gmail.com', '3435345', NULL, '80', 'english', NULL, 'Approved');
/*!40000 ALTER TABLE `request` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.service
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(1000) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.service: 6 rows
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` (`id`, `img_url`, `title`, `description`) VALUES
	(1, 'uploads/cardic.jpg', 'Cardiac Excellence', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(2, '', 'Cancer Treatment', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(3, '', 'Stroke Management', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(4, '', '24 / 7 Support', 'Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence Cardiac Excellence'),
	(5, 'uploads/inlinePreview1.jpg', 'bfbfjsb', 'jvbfdjvbj'),
	(6, 'uploads/photos_002.JPG', 'gdfghfghgf', 'hfghfgfgfgfg');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `system_vendor` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `live_appointment_type` varchar(100) DEFAULT NULL,
  `vat` varchar(100) DEFAULT NULL,
  `login_title` varchar(100) DEFAULT NULL,
  `logo` varchar(500) DEFAULT NULL,
  `invoice_logo` varchar(500) DEFAULT NULL,
  `payment_gateway` varchar(100) DEFAULT NULL,
  `sms_gateway` varchar(100) DEFAULT NULL,
  `codec_username` varchar(100) DEFAULT NULL,
  `codec_purchase_code` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.settings: 2 rows
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `system_vendor`, `title`, `address`, `phone`, `email`, `facebook_id`, `currency`, `language`, `discount`, `live_appointment_type`, `vat`, `login_title`, `logo`, `invoice_logo`, `payment_gateway`, `sms_gateway`, `codec_username`, `codec_purchase_code`, `hospital_id`) VALUES
	(11, 'KIDNEY HOPE', 'KIDNEY HOPE', 'Ciudad de Guatemala', '49504854', 'superadmin@hms.com', NULL, 'Q ', 'english', 'flat', NULL, NULL, NULL, 'uploads/fundanier1.png', NULL, 'PayPal', 'Twilio', '', '', 'superadmin'),
	(10, 'HOSPITAL', 'Hospital', 'Collegepara, Rajbari', '+880123456789', 'admin@hms.com', NULL, '$', 'spanish', 'flat', 'jitsi', NULL, NULL, 'uploads/150150Logo2.jpg', NULL, 'PayPal', 'Twilio', '', '', '466');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.slide
CREATE TABLE IF NOT EXISTS `slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `img_url` varchar(1000) DEFAULT NULL,
  `text1` varchar(500) DEFAULT NULL,
  `text2` varchar(500) DEFAULT NULL,
  `text3` varchar(500) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.slide: 2 rows
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;
INSERT INTO `slide` (`id`, `title`, `img_url`, `text1`, `text2`, `text3`, `position`, `status`) VALUES
	(1, 'Fundanier', 'uploads/1503411077revised-bhatia-homebanner-03.jpg', 'La Fundación para el Niño Enfermo Renal ', 'La Fundación para el Niño Enfermo Renal ', 'Hospital', '2', 'Active'),
	(2, 'Best Hospital management System', 'uploads/1707260345350542.jpg', 'Best Hospital management System', 'Best Hospital management System', 'Best Hospital management System', '1', 'Active');
/*!40000 ALTER TABLE `slide` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.sms
CREATE TABLE IF NOT EXISTS `sms` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `date` varchar(100) DEFAULT NULL,
  `message` varchar(1600) DEFAULT NULL,
  `recipient` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.sms: 0 rows
/*!40000 ALTER TABLE `sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.sms_settings
CREATE TABLE IF NOT EXISTS `sms_settings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `api_id` varchar(100) DEFAULT NULL,
  `sender` varchar(100) DEFAULT NULL,
  `authkey` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `sid` varchar(1000) DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `sendernumber` varchar(1000) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.sms_settings: 3 rows
/*!40000 ALTER TABLE `sms_settings` DISABLE KEYS */;
INSERT INTO `sms_settings` (`id`, `name`, `username`, `password`, `api_id`, `sender`, `authkey`, `user`, `sid`, `token`, `sendernumber`, `hospital_id`) VALUES
	(29, 'Twilio', NULL, NULL, NULL, NULL, NULL, '763', 'Enter_Twilio_SID', 'Enter_Twilio_Token_Password', 'Enter_Sender_Number', '466'),
	(28, 'MSG91', NULL, NULL, NULL, 'Enter_Sender_Number', 'Enter_Your_MSG91_Auth_Key', '763', NULL, NULL, NULL, '466'),
	(27, 'Clickatell', 'Enter_Your_ClickAtell_Username', '', 'Enter_Your_ClickAtell_Api _Id', NULL, NULL, '763', NULL, NULL, NULL, '466');
/*!40000 ALTER TABLE `sms_settings` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.template
CREATE TABLE IF NOT EXISTS `template` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `template` varchar(10000) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.template: 0 rows
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
/*!40000 ALTER TABLE `template` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.test_info
CREATE TABLE IF NOT EXISTS `test_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(100) DEFAULT NULL,
  `donor_id` varchar(100) DEFAULT NULL,
  `ant1` varchar(10) DEFAULT NULL,
  `ant2` varchar(10) DEFAULT NULL,
  `ant3` varchar(10) DEFAULT NULL,
  `ant4` varchar(10) DEFAULT NULL,
  `ant5` varchar(10) DEFAULT NULL,
  `ant6` varchar(10) DEFAULT NULL,
  `bloodgroup` varchar(10) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `birthdate` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `donor_id` (`donor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla hospital.test_info: 10 rows
/*!40000 ALTER TABLE `test_info` DISABLE KEYS */;
INSERT INTO `test_info` (`id`, `patient_id`, `donor_id`, `ant1`, `ant2`, `ant3`, `ant4`, `ant5`, `ant6`, `bloodgroup`, `weight`, `birthdate`, `sex`) VALUES
	(1, '256271', NULL, 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'O-', 50, '29-01-2021', 'Male'),
	(2, '118800', NULL, 'A1', 'A2', 'B1', 'B2', 'C1', 'C2', 'O-', 50, '01-01-1999', 'Male'),
	(3, '646905', NULL, 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'B-', 75, '07-01-2021', 'Male'),
	(4, NULL, '938994', 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'O-', 70, '10-02-2021', 'Male'),
	(5, '59300', NULL, 'AA', 'AA1', 'B1', 'B2', 'DD1', 'DD2', 'A+', 60, '22-12-2020', 'Male'),
	(6, NULL, '577685', 'AA', 'A1', 'BB', 'BB2', 'D1', 'D2', 'AB-', 65, '16-02-2021', 'Male'),
	(7, NULL, '628950', 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'O-', 50, '16-02-2021', 'Male'),
	(8, NULL, '45008', 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'O-', 50, '16-02-2021', 'Male'),
	(9, '158098', NULL, 'A1', 'A5', 'B6', 'B12', 'DR5', 'DR6', 'O-', 70, '01-07-2020', 'Male'),
	(10, NULL, '971836', 'A1', 'A2', 'B1', 'B2', 'D1', 'D2', 'AB-', 50, '18-02-2021', 'Male');
/*!40000 ALTER TABLE `test_info` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.time_schedule
CREATE TABLE IF NOT EXISTS `time_schedule` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `doctor` varchar(500) DEFAULT NULL,
  `weekday` varchar(100) DEFAULT NULL,
  `s_time` varchar(100) DEFAULT NULL,
  `e_time` varchar(100) DEFAULT NULL,
  `s_time_key` varchar(100) DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.time_schedule: 2 rows
/*!40000 ALTER TABLE `time_schedule` DISABLE KEYS */;
INSERT INTO `time_schedule` (`id`, `doctor`, `weekday`, `s_time`, `e_time`, `s_time_key`, `duration`, `hospital_id`) VALUES
	(108, '162', 'Friday', '11:00 AM', '11:45 PM', '132', '6', '466'),
	(109, '162', 'Thursday', '08:30 AM', '12:30 PM', '102', '12', '466');
/*!40000 ALTER TABLE `time_schedule` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.time_slot
CREATE TABLE IF NOT EXISTS `time_slot` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `doctor` varchar(100) DEFAULT NULL,
  `s_time` varchar(100) DEFAULT NULL,
  `e_time` varchar(100) DEFAULT NULL,
  `weekday` varchar(100) DEFAULT NULL,
  `s_time_key` varchar(100) DEFAULT NULL,
  `hospital_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2269 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.time_slot: 29 rows
/*!40000 ALTER TABLE `time_slot` DISABLE KEYS */;
INSERT INTO `time_slot` (`id`, `doctor`, `s_time`, `e_time`, `weekday`, `s_time_key`, `hospital_id`) VALUES
	(2240, '162', '11:00 AM', '11:30 AM', 'Friday', '132', '466'),
	(2241, '162', '11:30 AM', '12:00 AM', 'Friday', '138', '466'),
	(2242, '162', '12:00 AM', '12:30 PM', 'Friday', '144', '466'),
	(2243, '162', '12:30 PM', '01:00 PM', 'Friday', '150', '466'),
	(2244, '162', '01:00 PM', '01:30 PM', 'Friday', '156', '466'),
	(2245, '162', '01:30 PM', '02:00 PM', 'Friday', '162', '466'),
	(2246, '162', '02:00 PM', '02:30 PM', 'Friday', '168', '466'),
	(2247, '162', '02:30 PM', '03:00 PM', 'Friday', '174', '466'),
	(2248, '162', '03:00 PM', '03:30 PM', 'Friday', '180', '466'),
	(2249, '162', '03:30 PM', '04:00 PM', 'Friday', '186', '466'),
	(2250, '162', '04:00 PM', '04:30 PM', 'Friday', '192', '466'),
	(2251, '162', '04:30 PM', '05:00 PM', 'Friday', '198', '466'),
	(2252, '162', '05:00 PM', '05:30 PM', 'Friday', '204', '466'),
	(2253, '162', '05:30 PM', '06:00 PM', 'Friday', '210', '466'),
	(2254, '162', '06:00 PM', '06:30 PM', 'Friday', '216', '466'),
	(2255, '162', '06:30 PM', '07:00 PM', 'Friday', '222', '466'),
	(2256, '162', '07:00 PM', '07:30 PM', 'Friday', '228', '466'),
	(2257, '162', '07:30 PM', '08:00 PM', 'Friday', '234', '466'),
	(2258, '162', '08:00 PM', '08:30 PM', 'Friday', '240', '466'),
	(2259, '162', '08:30 PM', '09:00 PM', 'Friday', '246', '466'),
	(2260, '162', '09:00 PM', '09:30 PM', 'Friday', '252', '466'),
	(2261, '162', '09:30 PM', '10:00 PM', 'Friday', '258', '466'),
	(2262, '162', '10:00 PM', '10:30 PM', 'Friday', '264', '466'),
	(2263, '162', '10:30 PM', '11:00 PM', 'Friday', '270', '466'),
	(2264, '162', '11:00 PM', '11:30 PM', 'Friday', '276', '466'),
	(2265, '162', '08:30 AM', '09:30 AM', 'Thursday', '102', '466'),
	(2266, '162', '09:30 AM', '10:30 AM', 'Thursday', '114', '466'),
	(2267, '162', '10:30 AM', '11:30 AM', 'Thursday', '126', '466'),
	(2268, '162', '11:30 AM', '12:30 PM', 'Thursday', '138', '466');
/*!40000 ALTER TABLE `time_slot` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `hospital_ion_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=807 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.users: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `hospital_ion_id`) VALUES
	(1, '127.0.0.1', 'superadmin', '$2y$08$GclRTUhfRYv19X.qFyJ7KegFXr6CZq93.cZtxil5akzCEss1m2UrK', '', 'superadmin@hms.com', '', 'eX0.Bq6nP57EuXX4hJkPHO973e7a4c25f1849d3a', 1511432365, 'zCeJpcj78CKqJ4sVxVbxcO', 1268889823, 1613098830, 1, 'Admin', 'istrator', 'ADMIN', '0', NULL),
	(763, '45.251.231.70', 'Fundanier', '$2y$08$wR54EAOQ4tcon8tP6W6YAOEbKL5oXsn7jIpkyl8qeP158IL/abHDq', NULL, 'admin@hms.com', NULL, NULL, NULL, NULL, 1595923316, 1613605274, 1, NULL, NULL, NULL, NULL, NULL),
	(764, '45.251.231.70', 'Julian Ramirez', '$2y$08$9nBBXRUaEjneHqyHplrt7OEe1n7eMDLS3C/Ztp7kq6s76gS1fHbrm', NULL, 'patient@hms.com', NULL, NULL, NULL, NULL, 1595924679, 1600202983, 1, NULL, NULL, NULL, NULL, '763'),
	(765, '45.251.231.70', 'Dr. Carlos Dardon', '$2y$08$fMoSqdFzlemryEACC1LTvuXuNkIpTjkviovMrLfsmuYB0iR9m0Nrq', NULL, 'doctor@hms.com', NULL, NULL, NULL, NULL, 1595924765, 1603640667, 1, NULL, NULL, NULL, NULL, '763'),
	(767, '45.251.231.70', 'Mr Pharmacist', '$2y$08$7NA95vFwBNompkFy9nWOAu0V7l954eQsO/JgHSJmEuDAPQqTUDKMe', NULL, 'pharmacist@hms.com', NULL, NULL, NULL, NULL, 1595928739, 1600846048, 1, NULL, NULL, NULL, NULL, '763'),
	(770, '45.251.231.70', 'Mr Receptionist', '$2y$08$uQ8hjJRTGAEsshOFkS8acue03e.4h3MXQLwH79837vhqu7SH9U3TK', NULL, 'receptionist@hms.com', NULL, NULL, NULL, NULL, 1595929512, 1600753152, 1, NULL, NULL, NULL, NULL, '763'),
	(787, '103.231.160.47', 'Mr Accountant', '$2y$08$GVopabH96MmnQFKKqYCYJOjlr0kgUQUg7GOjsqnJ/.tlcQCfwg0cm', NULL, 'accountant@hms.com', NULL, NULL, NULL, NULL, 1600753981, 1603428846, 1, NULL, NULL, NULL, NULL, '763'),
	(789, '103.231.160.47', 'Mr Laboratorist', '$2y$08$B2WZyEiQdFCPndWw8//ZpuFXb7pc00nAiIU8g0S7TJAg5NLK3I5sK', NULL, 'laboratorist@hms.com', NULL, NULL, NULL, NULL, 1600845967, 1600845979, 1, NULL, NULL, NULL, NULL, '763'),
	(790, '103.231.160.47', 'Mrs Nurse', '$2y$08$RYAAuKhOF.zyefUPGwTHp.LH95LMMjO64et98EgHPpmzrbQY2tP82', NULL, 'nurse@hms.com', NULL, NULL, NULL, NULL, 1600846100, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(791, '::1', 'Juan Jose', '$2y$08$NcQLM1PkQOqOLmhlahb06OvPT5Bs1IkbIxl4Hn2vQb74mkceMMv5G', NULL, 'a@juango.com', NULL, NULL, NULL, NULL, 1608657463, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(792, '::1', 'josguerra99@gmail.com', '$2y$08$PU3TGBpE6GKlAfLNoYSAOuWIcBTwXl8pwLVfZDBVTUDGXQiVSvPfG', NULL, 'josguerra99@gmail.com', NULL, NULL, NULL, NULL, 1609280481, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(793, '::1', 'José Guerra', '$2y$08$uqJXPfZlrgjuA5cL7szUG.C6fo1KJd6UNCuiYOOA3qo06Ul6LYkCa', NULL, 'aaaa@hms.com', NULL, NULL, NULL, NULL, 1609280639, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(794, '::1', 'Paciente Editado', '$2y$08$Ib4zQzXRDWk08m5WwAMk8e5En0C9YAzN9FA0ZjniHZfkQuvCJXq3e', NULL, 'nuevo@hms.com', NULL, NULL, NULL, NULL, 1609282461, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(795, '::1', 'eeeeeeeeee', '$2y$08$22DmU9/lpqjX.8hCK2Q9GeVUGMqsZb4TbxjSr26php8QHl6tO4zKm', NULL, 'test1@hms.com', NULL, NULL, NULL, NULL, 1609377644, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(797, '::1', '', '0', NULL, '', NULL, NULL, NULL, NULL, 1609996970, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(798, '::1', '', '0', NULL, '', NULL, NULL, NULL, NULL, 1609996978, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(800, '::1', 'aaaa', '$2y$08$P6C3NtdwUO7GGSNoxcztDu74PW4DACkIUtr.NshLuUeruFEh5WECW', NULL, 'elcorreo@nuevo', NULL, NULL, NULL, NULL, 1610005153, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(801, '::1', 'José Guerra', '$2y$08$hIq.yA67WCULU5Yw0xq6Je5FAtdMBPm1i84T.bUZPFcL4EBE.5Gna', NULL, 'jnfas@fsdaf', NULL, NULL, NULL, NULL, 1610068579, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(802, '::1', 'EDITADOOO', '$2y$08$S/EeN/bSmAifC/v95H./xeiw/nCxWVdrZQoyBKDqzWtK4/PcEpofS', NULL, 'fsdafs@sgfasd', NULL, NULL, NULL, NULL, 1610674126, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(803, '::1', 'Paciente Nombre', '$2y$08$Y4uVG94AbkHh6PDqJCifUuK9rIVSmjvQ/XW.5IA3Xsl0TH3mcimi2', NULL, 'a@aaaa', NULL, NULL, NULL, NULL, 1611357859, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(804, '::1', 'Paciente Nombre1', '$2y$08$QOpaWfJb136MWCC7ljKqOeZ8qpcNakiNeqvSi4BJn3UoK2CgMmM..', NULL, 'a@aaaab', NULL, NULL, NULL, NULL, 1611357937, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(805, '::1', 'aaaa1', '$2y$08$nNzgKH/XWuEoQvvYucKyxeNF2XRwiOJzOLAgFwelQn3fVyiwhEI0.', NULL, 'admidfsadn@hms.com', NULL, NULL, NULL, NULL, 1611358154, NULL, 1, NULL, NULL, NULL, NULL, '763'),
	(806, '::1', 'bb', '$2y$08$MTzYn0Jt1PH6NlMCKjZvUeV7IbwKBJHbSrwXD9AH8dmZuSmHcMl6a', NULL, 'b@bbb', NULL, NULL, NULL, NULL, 1611360705, NULL, 1, NULL, NULL, NULL, NULL, '763');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=809 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.users_groups: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(765, 763, 11),
	(766, 764, 5),
	(767, 765, 4),
	(769, 767, 7),
	(772, 770, 10),
	(789, 787, 3),
	(791, 789, 8),
	(792, 790, 6),
	(793, 791, 5),
	(794, 792, 5),
	(795, 793, 5),
	(796, 794, 5),
	(797, 795, 5),
	(799, 797, 5),
	(800, 798, 5),
	(802, 800, 5),
	(803, 801, 5),
	(804, 802, 5),
	(805, 803, 5),
	(806, 804, 5),
	(807, 805, 5),
	(808, 806, 5);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

-- Volcando estructura para tabla hospital.website_settings
CREATE TABLE IF NOT EXISTS `website_settings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `logo` varchar(1000) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `emergency` varchar(100) DEFAULT NULL,
  `support` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `block_1_text_under_title` varchar(500) DEFAULT NULL,
  `service_block__text_under_title` varchar(500) DEFAULT NULL,
  `doctor_block__text_under_title` varchar(500) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `twitter_id` varchar(100) DEFAULT NULL,
  `google_id` varchar(100) DEFAULT NULL,
  `youtube_id` varchar(100) DEFAULT NULL,
  `skype_id` varchar(100) DEFAULT NULL,
  `x` varchar(100) DEFAULT NULL,
  `twitter_username` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hospital.website_settings: 1 rows
/*!40000 ALTER TABLE `website_settings` DISABLE KEYS */;
INSERT INTO `website_settings` (`id`, `title`, `logo`, `address`, `phone`, `emergency`, `support`, `email`, `currency`, `block_1_text_under_title`, `service_block__text_under_title`, `doctor_block__text_under_title`, `facebook_id`, `twitter_id`, `google_id`, `youtube_id`, `skype_id`, `x`, `twitter_username`) VALUES
	(1, 'Fundanier', 'uploads/fundanier2.png', 'Ciudad de Guatemala', '545454545', '545454545', '545454545', 'admin@demo.com', 'Q', 'a Fundación para el Niño Enfermo Renal -Fundanier-', 'Fundanier se ha constituido en la única entidad a nivel nacional, especializada en la atención de niños y niñas con enfermedades de riñón.', 'Ser el mejor servicio de nefrología pediátrica a nivel de Centroamérica y el Caribe, generadores de conocimiento y formador de recurso humano con impacto en futuras generaciones.', 'https://www.facebook.com/fundanier', 'https://www.twitter.com/fundanier', 'https://www.google.com/fundanier', 'https://www.youtube.com/fundanier', 'https://www.skype.com/fundanier', NULL, 'Fundanier');
/*!40000 ALTER TABLE `website_settings` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
