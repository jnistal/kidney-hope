-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.31 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para kidney_hope
CREATE DATABASE IF NOT EXISTS `kidney_hope` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kidney_hope`;

-- Volcando estructura para tabla kidney_hope.antigeno
CREATE TABLE IF NOT EXISTS `antigeno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` varchar(11) NOT NULL,
  `nombre` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `antigeno_nombre` (`nombre`),
  KEY `antigeno_persona` (`id_persona`)
) ENGINE=MyISAM AUTO_INCREMENT=120145 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla kidney_hope.donador
CREATE TABLE IF NOT EXISTS `donador` (
  `id` varchar(11) NOT NULL,
  `id_paciente` varchar(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `tipo_sangre` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paciente` (`id_paciente`),
  KEY `donador_tipo_sangre` (`tipo_sangre`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla kidney_hope.paciente
CREATE TABLE IF NOT EXISTS `paciente` (
  `id` varchar(11) NOT NULL,
  `tipo_sangre` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `paciente_tipo_sangre` (`tipo_sangre`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
