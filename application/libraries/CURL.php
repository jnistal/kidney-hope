<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Curl
{
    public function host()
    {
        return 'http://localhost:3001';
    }


    public function curlDelete($url)
    {
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "DELETE"
        );


        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);

        $info = curl_getinfo($ch);
        http_response_code($info['http_code']);
        curl_close($ch);
        return;
    }


    public function post($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,   $this->host() . $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        http_response_code($info['http_code']);
        curl_close($ch);
        return $response;
    }


    public function get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,   $this->host() . $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data, true);
    }
}