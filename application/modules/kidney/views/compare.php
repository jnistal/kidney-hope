<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<style>
.btn-container {
    display: flex;
    margin: 16px;
    margin-left: 22px;
    position: absolute;
    bottom: 0;
}

.btn:focus {
    outline-width: 3px !important;
    outline-style: dashed !important;
    outline-color: #077AF6 !important;

}

.btn-next {
    margin: 5px;
}

.form-container {
    position: relative;
    min-height: 600px;
}

.form-footer {
    height: 60px;
}

.content {
    margin: 20px;
}

.title-container {
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
}

.title {
    font-weight: bold;
    text-transform: uppercase;
    font-size: 35px;
    color: #545454;

}

.subtitle {
    font-size: 20px;

}
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-md-6">


                <div class="title-container">
                    <h3 class="title"><?php echo $patient->patient_id; ?></h3>
                    <h5 class="subtitle">Paciente</h5>
                </div>
                <br />


                <div class="mb-3">
                    <label for="bloodInput" class="form-label">Tipo de Sangre</label>
                    <input type="text" class="form-control" id="bloodInput" aria-describedby="name" disabled
                        value="<?php if (!empty($patient->bloodgroup)) echo $patient->bloodgroup; ?>">

                </div>

                <div class="mb-3">
                    <label for="birthdateInput" class="form-label">Fecha de Nacimiento</label>
                    <input type="text" class="form-control form-control-inline input-medium default-date-picker"
                        name="birthdateInput" id="birthdateInput" autocomplete="new-password" disabled placeholder=""
                        value="<?php if (!empty($patient->birthdate)) echo $patient->birthdate; ?>">
                </div>


                <div class="mb-3">

                    <label for="sexInput" class="form-label">Género</label>
                    <select class="form-control m-bot15" name="sexInput" id='sexInput' value='' disabled>
                        <option value="Male"
                            <?php if (!empty($patient->sex) && $patient->sex == 'Male') echo 'selected'; ?>> Male
                        </option>

                        <option value="Female"
                            <?php if (!empty($setval) && (set_value('sex') == 'Female')) echo 'selected'; ?>> Female
                        </option>


                        <option value="Others"
                            <?php if (!empty($patient->sex) && $patient->sex == 'Others') echo 'selected'; ?>> Others
                        </option>

                    </select>


                </div>






                <div class="mb-3">
                    <label for="weightInput" class="form-label">Peso (kg)</label>
                    <input type="number" class="form-control" id="weightInput" aria-describedby="name"
                        value="<?php if (!empty($patient->weight)) echo $patient->weight; ?>" disabled>
                </div>

                <div class="mb-3">
                    <label for="ant1Input" class="form-label">Antigeno 1</label>
                    <input type="text" class="form-control" id="ant1Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant1)) echo $patient->ant1; ?>" disabled>
                </div>

                <div class="mb-3">
                    <label for="ant2Input" class="form-label">Antigeno 2</label>
                    <input type="text" class="form-control" id="ant2Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant2)) echo $patient->ant2; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant3Input" class="form-label">Antigeno 3</label>
                    <input type="text" class="form-control" id="ant3Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant3)) echo $patient->ant3; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant4Input" class="form-label">Antigeno 4</label>
                    <input type="text" class="form-control" id="ant4Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant4)) echo $patient->ant4; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant5Input" class="form-label">Antigeno 5</label>
                    <input type="text" class="form-control" id="ant5Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant5)) echo $patient->ant5; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant6Input" class="form-label">Antigeno 6</label>
                    <input type="text" class="form-control" id="ant6Input" aria-describedby="name"
                        value="<?php if (!empty($patient->ant6)) echo $patient->ant6; ?>" disabled>
                </div>





            </div>
            <div class="col-md-6">


                <div class="title-container">
                    <h3 class="title"><?php echo $donor->donor_id; ?></h3>

                    <h5 class="subtitle">Donador</h5>
                </div>
                <br />


                <div class="mb-3">
                    <label for="bloodInput" class="form-label">Tipo de Sangre</label>
                    <input type="text" class="form-control" id="bloodInput" aria-describedby="name" disabled
                        value="<?php if (!empty($donor->bloodgroup)) echo $donor->bloodgroup; ?>">
                </div>

                <div class="mb-3">
                    <label for="birthdateInput" class="form-label">Fecha de Nacimiento</label>
                    <input type="text" class="form-control form-control-inline input-medium default-date-picker"
                        name="birthdateInput" id="birthdateInput" autocomplete="new-password" disabled placeholder=""
                        value="<?php if (!empty($donor->birthdate)) echo $donor->birthdate; ?>">
                </div>


                <div class="mb-3">

                    <label for="sexInput" class="form-label">Género</label>
                    <select class="form-control m-bot15" name="sexInput" id='sexInput' value='' disabled>
                        <option value="Male"
                            <?php if (!empty($donor->sex) && $donor->sex == 'Male') echo 'selected'; ?>> Male
                        </option>

                        <option value="Female"
                            <?php if (!empty($setval) && (set_value('sex') == 'Female')) echo 'selected'; ?>> Female
                        </option>


                        <option value="Others"
                            <?php if (!empty($donor->sex) && $donor->sex == 'Others') echo 'selected'; ?>> Others
                        </option>

                    </select>


                </div>






                <div class="mb-3">
                    <label for="weightInput" class="form-label">Peso (kg)</label>
                    <input type="number" class="form-control" id="weightInput" aria-describedby="name"
                        value="<?php if (!empty($donor->weight)) echo $donor->weight; ?>" disabled>
                </div>

                <div class="mb-3">
                    <label for="ant1Input" class="form-label">Antigeno 1</label>
                    <input type="text" class="form-control" id="ant1Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant1)) echo $donor->ant1; ?>" disabled>
                </div>

                <div class="mb-3">
                    <label for="ant2Input" class="form-label">Antigeno 2</label>
                    <input type="text" class="form-control" id="ant2Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant2)) echo $donor->ant2; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant3Input" class="form-label">Antigeno 3</label>
                    <input type="text" class="form-control" id="ant3Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant3)) echo $donor->ant3; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant4Input" class="form-label">Antigeno 4</label>
                    <input type="text" class="form-control" id="ant4Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant4)) echo $donor->ant4; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant5Input" class="form-label">Antigeno 5</label>
                    <input type="text" class="form-control" id="ant5Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant5)) echo $donor->ant5; ?>" disabled>
                </div>


                <div class="mb-3">
                    <label for="ant6Input" class="form-label">Antigeno 6</label>
                    <input type="text" class="form-control" id="ant6Input" aria-describedby="name"
                        value="<?php if (!empty($donor->ant6)) echo $donor->ant6; ?>" disabled>
                </div>





            </div>
        </div>


        <!-- page end-->
    </section>
</section>

<div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true" data-delay=3000
    id="toast">
    <div class="toast-header">
        Toast Header
    </div>
    <div class="toast-body">
        Some text inside the toast body
    </div>
</div>

<!--main content end-->
<!--footer start-->
<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>