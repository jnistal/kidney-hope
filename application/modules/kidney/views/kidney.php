<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<style>
/* .form-group {
    display: flex;
}

.form-label {
    width: 150px;
} */
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <br />
        <br />

        <input placeholder="Número de afiliación" type="text" id="patient_id" name="patient_id"
            style="height:40px;min-width:400px;max-width:100%;" onkeydown="searchPatientOnEnter(this)" />

        <button class="btn btn-info" id="<?php echo 'info-' . $patient->id; ?>" onClick='search()'
            style="height:40px">Buscar</button>
        <br />
        <br />
        <div class=" patients-container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Número de afiliación</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($patients as $patient) : ?>
                    <tr>
                        <th scope="row"><?php echo $patient->patient_id; ?></th>
                        <td><?php echo $patient->name; ?></td>
                        <td>

                            <button class="btn btn-info" id="<?php echo 'info-' . $patient->id; ?>"
                                onClick='openViewPatient(<?php echo json_encode($patient); ?>)'>Ver</button>
                        </td>
                    </tr>
                    <?php endforeach; ?>


                </tbody>
            </table>


            <?php if (count($patient) == 0) : ?>

            <h5>No se encontraron resultados</h5>
            <?php endif ?>


        </div>
        <!-- page end-->
    </section>
</section>

<div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true" data-delay=3000
    id="toast">
    <div class="toast-header">
        Toast Header
    </div>
    <div class="toast-body">
        Some text inside the toast body
    </div>
</div>

<!--main content end-->
<!--footer start-->
<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



<!-- Helpers -->
<script>
/**
 * Redirige a la vista del paciente
 */
function openViewPatient(patient) {
    $(location).attr('href', `kidney/patientCandidates?id=${patient.patient_id}`);
}


function search() {
    $(location).attr('href', `kidney?id=${document.getElementById('patient_id').value}`);


}


function searchPatientOnEnter(value) {
    if (event.key === 'Enter') {
        search();
    }

}
</script>