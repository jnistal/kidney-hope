<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<style>
.btn-container {
    display: flex;
    margin: 16px;
    margin-left: 22px;
    position: absolute;
    bottom: 0;
}

.btn:focus {
    outline-width: 3px !important;
    outline-style: dashed !important;
    outline-color: #077AF6 !important;

}

.btn-next {
    margin: 5px;
}

.form-container {
    position: relative;
    min-height: 600px;
}

.form-footer {
    height: 60px;
}

.content {
    margin: 20px;
}
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel form-container">
            <header class="panel-heading">
                <?php
                if (!empty($donor->id))
                    echo 'Editar Donador';
                else
                    echo 'Agregar Donador';
                ?>
            </header>


            <div class="">

                <!-- Información del paciente-->
                <div id="patient-info" class="content" role="tabpanel" aria-labelledby="patient-info-trigger">

                    <div class="form-group">
                        <label for="patient_id" class="form-label">Número de afiliación del paciente</label>
                        <input type="text" class="form-control" id="patient_id" name="patient_id"
                            value="<?php echo $donor->patient_id ?>">
                    </div>

                    <div class="form-group">
                        <label for="birthdate" class="form-label"><?php echo lang('birth_date'); ?></label>
                        <input class="form-control form-control-inline input-medium default-date-picker" id="birthdate"
                            type="text" name="birthdate"
                            value="<?php if (!empty($donor->birthdate)) echo $donor->birthdate; ?>" placeholder="">
                    </div>


                    <div class="form-group">
                        <label class="form-label"><?php echo lang('sex'); ?></label>
                        <select class="form-control m-bot15" name="sex" value=''>
                            <option value="Male" <?php
                                                    if (
                                                        !empty($setval) && (set_value('sex') == 'Male') ||
                                                        !empty($donor->sex) && $donor->sex == 'Male'
                                                    ) echo 'selected';
                                                    ?>> Male
                            </option>

                            <option value="Female" <?php
                                                    if (
                                                        !empty($setval) && (set_value('sex') == 'Female') ||
                                                        !empty($donor->sex) && $donor->sex == 'Female'
                                                    ) echo 'selected';
                                                    ?>> Female
                            </option>


                            <option value="Others" <?php
                                                    if (
                                                        !empty($setval) && (set_value('sex') == 'Others') ||
                                                        !empty($donor->sex) && $donor->sex == 'Others'
                                                    ) echo 'selected';
                                                    ?>> Others
                            </option>

                        </select>
                    </div>


                    <div class="form-group">
                        <label for="boodgroup"><?php echo lang('blood_group'); ?></label>
                        <select class="form-control m-bot15" name="bloodgroup" value=''>
                            <?php foreach ($groups as $group) { ?>
                            <option value="<?php echo $group->group; ?>" <?php
                                                                                if (!empty($setval) && $group->group == set_value('bloodgroup')) echo 'selected';
                                                                                if (!empty($patient->bloodgroup) && $group->group == $patient->bloodgroup) echo 'selected';
                                                                                ?>>
                                <?php echo $group->group; ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>


                </div>
                <!-- Cuenta-->

            </div>

            <div class="form-footer"></div>

            </div>

            <div class="btn-container">
                <button class="btn btn-info btn-next" id="add-btn">Agregar Donador</button>
                <button class="btn btn-info btn-next" id="edit-btn">Editar Donador</button>

            </div>
        </section>
        <!-- page end-->
    </section>
</section>

<div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true" data-delay=3000
    id="toast">
    <div class="toast-header">
        Toast Header
    </div>
    <div class="toast-body">
        Some text inside the toast body
    </div>
</div>

<!--main content end-->
<!--footer start-->
<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



<!-- Helpers -->
<script>
const urlParams = new URLSearchParams(window.location.search);
const currentId = urlParams.get('id');
const patientId = urlParams.get('patient_id');

let isEditing = false;
if (currentId) isEditing = true;
</script>

<!-- Manejar botones de stepper (anterior, siguiente, añadir, editar) 
     Además maneja el autofocus de cada paso del stepper-->
<script>
$(document).ready(() => {

    //Desactivar los botones inicialmente
    $("#add-btn").hide();
    $("#edit-btn").hide();
    $("#patient_id").focus()

    if (isEditing) $("#patient_id").prop("disabled", true);
    //Id del boton de accion (agregar o editar)
    let actionBtnId = !isEditing ? "#add-btn" : "#edit-btn";
    if (patientId) $("#patient_id").val(patientId);
    $(actionBtnId).show();


    document.getElementById('add-btn').onclick = () => {
        if (!isEditing) addPatient()
    };

    document.getElementById('edit-btn').onclick = () => {
        if (isEditing) addPatient();
    };

});
</script>


<!-- Añadir y editar pacientes -->

<script>
/**
 * Obtiene los datos del formulario 
 */
function getFormData() {

    const formData = new FormData();
    formData.append("patient_id", document.getElementsByName("patient_id")[0].value);
    // formData.append("name", document.getElementsByName("name")[0].value);
    // formData.append("address", document.getElementsByName("address")[0].value);
    // formData.append("phone", document.getElementsByName("phone")[0].value);
    formData.append("birthdate", document.getElementsByName("birthdate")[0].value);
    formData.append("sex", document.getElementsByName("sex")[0].value);
    formData.append("bloodgroup", document.getElementsByName("bloodgroup")[0].value);
    if (isEditing) formData.append("id", currentId);
    return formData;
}




/**
 * Maneja los errores y los notifica
 */
function notifyError(errorId) {
    switch (errorId) {
        case "form_error":
            toastr.error("Datos no ingresados correctamente");
            break;
        case "this_email_address_is_already_registered":
            toastr.error("Correo electrónico ya se encuentra registrado");
            break;
        case "invalid_patient":
            toastr.error("Paciente no existe");
            break;
        default:
            toastr.error("Error");
            break;
    }
}


/**
 * Realiza una petición para añadir un nuevo paciente
 */
async function addPatient() {
    let addBtn = !isEditing ? document.getElementById("add-btn") : document.getElementById("edit-btn");
    addBtn.disabled = true;


    try {
        const formData = getFormData();

        const response = await axios.post("/kidney/addDonorApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });


        window.location.href = (
            `/kidney/patientCandidates?id=${document.getElementsByName("patient_id")[0].value}`);
    } catch (error) {
        console.log(error)
        addBtn.disabled = false;

        if (error.response) {

            let errorId = error.response.data.error;
            notifyError(errorId);
        }
    }
}
</script>