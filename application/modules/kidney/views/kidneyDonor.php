<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="common/css/kidney/kidney.css">


<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="patients-container">



            <div class="row">
                <div class="col-md-2 patient-info-container">
                    <h2><?php echo $donor->name; ?></h2>
                    <button class="btn btn-primary" id="edit-donor-btn">Editar Donador</button>
                    <button class="btn btn-primary">Información Médica</button>

                </div>

                <div class="col-md-10 compatibility-container">