<div class="mb-3">
    <label for="bloodInput" class="form-label">Tipo de Sangre</label>
    <input type="text" class="form-control" id="bloodInput" aria-describedby="name" disabled
        value="<?php if (!empty($test->bloodgroup)) echo $test->bloodgroup;
                                                                                                    else if (!empty($patient->bloodgroup)) echo $patient->bloodgroup; ?>">
</div>

<div class="mb-3">
    <label for="birthdateInput" class="form-label">Fecha de Nacimiento</label>
    <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="birthdateInput"
        id="birthdateInput" autocomplete="new-password" disabled placeholder="" value="<?php if (!empty($test->birthdate)) echo $test->birthdate;
                                                                                                                                                                                                        else if (!empty($patient->birthdate)) echo $patient->birthdate; ?>
        ">
</div>


<div class="mb-3">
    <label for="sexInput" class="form-label">Género</label>
    <select class="form-control m-bot15" name="sexInput" id='sexInput' value='' disabled>
        <option value="Male" <?php
                                if (
                                    !empty($setval) && (set_value('sex') == 'Male') ||
                                    !empty($patient->sex) && $patient->sex == 'Male'
                                ) echo 'selected';
                                ?>> Male
        </option>

        <option value="Female" <?php
                                if (
                                    !empty($setval) && (set_value('sex') == 'Female') ||
                                    !empty($patient->sex) && $patient->sex == 'Female'
                                ) echo 'selected';
                                ?>> Female
        </option>


        <option value="Others" <?php
                                if (
                                    !empty($setval) && (set_value('sex') == 'Others') ||
                                    !empty($patient->sex) && $patient->sex == 'Others'
                                ) echo 'selected';
                                ?>> Others
        </option>

    </select>


</div>






<div class="mb-3">
    <label for="weightInput" class="form-label">Peso (kg)</label>
    <input type="number" class="form-control" id="weightInput" aria-describedby="name"
        value="<?php if (!empty($test->weight)) echo $test->weight; ?>">
</div>

<div class="mb-3">
    <label for="ant1Input" class="form-label">Antigeno 1</label>
    <input type="text" class="form-control" id="ant1Input" aria-describedby="name"
        value="<?php if (!empty($test->ant1)) echo $test->ant1; ?>">
</div>

<div class="mb-3">
    <label for="ant2Input" class="form-label">Antigeno 2</label>
    <input type="text" class="form-control" id="ant2Input" aria-describedby="name"
        value="<?php if (!empty($test->ant2)) echo $test->ant2; ?>">
</div>


<div class="mb-3">
    <label for="ant3Input" class="form-label">Antigeno 3</label>
    <input type="text" class="form-control" id="ant3Input" aria-describedby="name"
        value="<?php if (!empty($test->ant3)) echo $test->ant3; ?>">
</div>


<div class="mb-3">
    <label for="ant4Input" class="form-label">Antigeno 4</label>
    <input type="text" class="form-control" id="ant4Input" aria-describedby="name"
        value="<?php if (!empty($test->ant4)) echo $test->ant4; ?>">
</div>


<div class="mb-3">
    <label for="ant5Input" class="form-label">Antigeno 5</label>
    <input type="text" class="form-control" id="ant5Input" aria-describedby="name"
        value="<?php if (!empty($test->ant5)) echo $test->ant5; ?>">
</div>


<div class="mb-3">
    <label for="ant6Input" class="form-label">Antigeno 6</label>
    <input type="text" class="form-control" id="ant6Input" aria-describedby="name"
        value="<?php if (!empty($test->ant6)) echo $test->ant6; ?>">
</div>

<br />


<button class="btn btn-info" onClick='addPatientExam(<?php echo json_encode($patient); ?>)' id="saveBtn">Guardar
    Información</button>