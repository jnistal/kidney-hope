</div>
</div>


</div>
<!-- page end-->
</section>
</section>

<div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true" data-delay=3000
    id="toast">
    <div class="toast-header">
        Toast Header
    </div>
    <div class="toast-body">
        Some text inside the toast body
    </div>
</div>

<!--main content end-->
<!--footer start-->
<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script>
const urlParams = new URLSearchParams(window.location.search);
const currentId = urlParams.get('id');

document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById('view-patient-info-btn').addEventListener("click", openInfoView);
    document.getElementById('view-patient-medical-info-btn').addEventListener("click", openMedicalInfoView);
    document.getElementById('view-patient-candidates-btn').addEventListener("click", openCandidatesView);
});

function openInfoView() {
    window.location.href = (`/patient/parents?id=${currentId}`);
}

function openMedicalInfoView() {
    window.location.href = (`/kidney/patientMedicalInfo?id=${currentId}`);
}

function openCandidatesView() {
    window.location.href = (`/kidney/patientCandidates?id=${currentId}`);
}
</script>