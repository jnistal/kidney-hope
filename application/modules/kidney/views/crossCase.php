<link rel="stylesheet" href="common/css/kidney/crossCase.css">



<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="images-container">
            <span class="hope-number"><?php echo  $case_info['compatibility']; ?></span>
            <span class="hope-title">Hope Number</span>


            <div class="text-container text-1">
                <span class="person-title patient-a-title">Paciente A</span>
                <span class="person-subtitle patient-a-subtitle"><?php echo  $case_info['patientA']['id']; ?></span>
            </div>

            <div class="text-container text-2">
                <span class="person-title patient-a-title">Paciente B</span>
                <span class="person-subtitle patient-a-subtitle"><?php echo  $case_info['patientB']['id']; ?></span>
            </div>

            <div class="text-container text-3">
                <span class="person-title patient-a-title">Donador A</span>
                <span class="person-subtitle patient-a-subtitle"><?php echo  $case_info['donorA']['id']; ?></span>
            </div>

            <div class="text-container text-4">
                <span class="person-title patient-a-title">Donador B</span>
                <span class="person-subtitle patient-a-subtitle"><?php echo  $case_info['donorB']['id']; ?></span>
            </div>


            <img class="lines" src="common/img/kidney/crossCaseLines.svg" alt="">

            <button class="button main-btn">Dar Seguimiento</button>

            <div class="buttons-container">
                <button class="button caso-1-btn"
                    onClick='redirectToCompare(<?php echo json_encode(array('id' => $case_info['donorB']["id"], 'patient_id' => $case_info['patientA']["id"])); ?>)'>Ver
                    Caso 1</button>
                <button class="button caso-2-btn"
                    onClick='redirectToCompare(<?php echo json_encode(array('id' => $case_info['donorA']["id"], 'patient_id' => $case_info['patientB']["id"])); ?>)'>Ver
                    Caso 2</button>
            </div>
        </div>
    </section>
</section>


<script>
function redirectToCompare(donor) {
    $(location).attr('href', `kidney/compare?donor_id=${donor.id}&patient_id=${donor.patient_id}`);
}
</script>