<h3>Donantes</h3>
<div>

    <button class="btn btn-info" onClick='redirectToAddDonor(<?php echo json_encode($patient); ?>)'>Agregar
        Donador</button>
</div>
<br />
<div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Compatibilidad</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($donors as $donor) : ?>
                <tr>
                    <td> <?php echo $donor->id; ?></td>
                    <td> <?php echo $donor->name; ?></td>
                    <td> <?php echo $compatibilities[$donor->id]; ?></td>
                    <td>
                        <button class="btn btn-primary" onClick=' redirectToExam(<?php echo json_encode($donor); ?>)'>Información Médica</button>


                        <?php if (is_numeric($compatibilities[$donor->id])) : ?>
                            <button class="btn btn-primary" onClick=' redirectToCompare(<?php echo json_encode($donor); ?>)'>Comparar</button>
                        <?php endif; ?>
                    </td>
                </tr>

            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<br />
<h3>Busqueda Cruzada</h3>

<br />
<table class="table">
    <thead>
        <tr>
            <th scope="col">Id Donante</th>
            <th scope="col">Id Paciente B</th>
            <th scope="col">Id Donante B</th>
            <th scope="col">Hope Number</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($cases as $case) : ?>
            <tr>
                <td> <?php echo $case['donorA']["id"]; ?></td>
                <td> <?php echo $case['patientB']["id"]; ?></td>
                <td> <?php echo $case['donorB']["id"]; ?></td>
                <td> <?php echo $case['compatibility']; ?></td>

                <td>


                    <button class="btn btn-primary" onClick='redirectToCrossCase(<?php echo json_encode(array('donor_a_id' => $case['donorA']["id"], 'donor_b_id' => $case['donorB']["id"])); ?>)'>Comparar</button>

                </td>
            </tr>

        <?php endforeach; ?>

    </tbody>
</table>




<script>
    function redirectToExam(donor) {
        $(location).attr('href', `kidney/donorMedicalInfo?id=${donor.id}`);
    }


    function redirectToCompare(donor) {
        $(location).attr('href', `kidney/compare?donor_id=${donor.id}&patient_id=${donor.patient_id}`);
    }

    function redirectToCrossCase(donor) {

        $(location).attr('href', `kidney/crossCase?donor_a_id=${donor.donor_a_id}&donor_b_id=${donor.donor_b_id}`);
    }

    function redirectToAddDonor(patient) {
        $(location).attr('href', `kidney/addDonor?patient_id=${patient.patient_id}`);
    }

    async function getCompatibility() {

    }
</script>