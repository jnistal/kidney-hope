<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="common/css/kidney/kidney.css">


<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="patients-container">



            <div class="row">
                <div class="col-md-2 patient-info-container">
                    <img class="profile-picture" width="200" height="200"
                        src="<?php if (!empty($patient->img_url)) echo $patient->img_url;
                                                                                else echo '/hospital/common/img/blank-profile.jpg'; ?>" />
                    <h2 class="patient-name" id="patient-name"><?php echo $patient->name; ?></h2>
                    <button class="btn btn-primary" id="view-patient-info-btn">Ver Información</button>
                    <button class="btn btn-primary" id="view-patient-medical-info-btn">Información Médica</button>
                    <button class="btn btn-primary" id="view-patient-candidates-btn"> Candidatos</button>
                </div>

                <div class="col-md-10 compatibility-container">