<?php

/**
 * CONTROLADOR: kidney
 * José Guerra
 * 
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kidney extends MX_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('kidney_model');
        $this->load->model('patient/patient_model');
        $this->load->library('curl');
        $this->load->model('donor/donor_model');
    }

    /*****************************      FRONTEND        *****************************/
    public function index()
    {
        $patient_id = $this->input->get('id');
        $data['patients'] = $this->kidney_model->getPatientsByDoctor('doctor_id_aqui', $patient_id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('kidney', $data);
        $this->load->view('home/footer'); // just the header file
    }


    public function patient()
    {
        $patient_id = $this->input->get('id');
        $data['patient'] = $this->patient_model->getPatientByPatientId($patient_id);
        $this->load->view('home/dashboard');
        $this->load->view('kidneyPatient', $data);
        $this->load->view('kidneyPatientFooter');
        $this->load->view('home/footer');
    }

    public function patientCandidates()
    {
        $patient_id = $this->input->get('id');
        $data['patient'] = $this->patient_model->getPatientByPatientId($patient_id);
        $this->load->view('home/dashboard');
        $this->load->view('kidneyPatient', $data);


        //Para ver los candidatos, primero es necesario tener la informacion médica ingresada del paciente
        //En caso de no tener esos examenes ingresados, se mostrara ese mensaje
        $data['test'] = $this->kidney_model->getPatientTest($patient_id);


        if (empty($data['test'])) {
            $this->load->view('kidneyPatientNoExams');
            $this->load->view('kidneyPatientFooter');
            $this->load->view('home/footer');
            return;
        }


        //Luego hay que encontrar los donadores que tambien tengan sus examenes médicos ingresados
        //Y colocar en la tabla de donadores, esos examenes
        $data['donors'] = $this->kidney_model->getDonors($patient_id);

        $compatibilities = array();
        foreach ($data['donors'] as $donor) {
            $val = $this->getCompatibility($donor->id);

            if (empty($val['err'])) {
                $compatibilities += [$donor->id => $val['compatibility']];
            } else {
                $compatibilities += [$donor->id => '---- '];
            }
        }

        $data['compatibilities'] = $compatibilities;
        //   echo json_encode($data['donors']);


        //Por ultimo se debe traer los casos cruzados de cada donador
        $data['cases'] = $this->getCases($data['patient']->patient_id);


        $this->load->view('patientCandidates', $data);
        $this->load->view('kidneyPatientFooter');
        $this->load->view('home/footer');
    }


    public function compare()
    {
        $data['patient'] = $this->kidney_model->getPatientTest($this->input->get('patient_id'));
        $data['donor'] = $this->kidney_model->getDonorTest($this->input->get('donor_id'));
        $data['groups'] = $this->donor_model->getBloodBank();

        $this->load->view('home/dashboard');
        $this->load->view('compare', $data);
        $this->load->view('home/footer');
    }

    public function crossCase()
    {

        $donor_a_id = $this->input->get('donor_a_id');
        $donor_b_id = $this->input->get('donor_b_id');

        $data['case_info'] = $this->getCaseInfo($donor_a_id, $donor_b_id);

        $this->load->view('home/dashboard');
        $this->load->view('crossCase', $data);
        $this->load->view('home/footer');
    }

    public function addDonor()
    {
        $data['donor'] = $this->kidney_model->getDonor($this->input->get('id')); //ESTOY EDITANDO
        $data['groups'] = $this->donor_model->getBloodBank();

        $this->load->view('home/dashboard');
        $this->load->view('addDonor', $data);
        $this->load->view('home/footer');
    }

    public function patientMedicalInfo()
    {
        $data['patient'] = $this->patient_model->getPatientByPatientId($this->input->get('id'));
        $this->load->view('home/dashboard');
        $this->load->view('kidneyPatient', $data);
        $data['test'] = $this->kidney_model->getPatientTest($data['patient']->patient_id);
        $this->load->view('kidneyPatientExams', $data);
        $this->load->view('kidneyPatientFooter');
        $this->load->view("kidneyPatientExamsScripts", $data);
        $this->load->view('home/footer');
    }

    public function donorMedicalInfo()
    {
        $data['patient'] = $this->kidney_model->getDonor($this->input->get('id'));
        $data['donor'] = $data['patient'];
        $this->load->view('home/dashboard');
        $this->load->view('kidneyDonor', $data);
        $data['test'] = $this->kidney_model->getDonorTest($data['patient']->id);
        $this->load->view('kidneyPatientExams', $data);
        $this->load->view('kidneyDonorFooter');
        $this->load->view("kidneyDonorExamsScripts", $data);
        $this->load->view('home/footer');
    }

    public function patientInfo()
    {
        $data['patient'] = $this->patient_model->getPatientByPatientId($this->input->get('id'));
    }



    /*****************************      BACKEND        *****************************/


    private function errorResponse($error)
    {
        http_response_code(500);
        header('Content-Type: application/json');
        $response = array('error' => $error);
        echo json_encode($response);
    }

    private function isPost()
    {
        //Solo permitir peticiones POST
        if ($this->input->server('REQUEST_METHOD') !== 'POST') {
            $this->errorResponse('non_post_request');
            return false;
        }

        return true;
    }



    private function editDonorMedicalInfo($donor_id, $data)
    {


        $donor = $this->kidney_model->getDonor($donor_id);
        $test = $this->kidney_model->getDonorTest($donor_id);



        //ACTUALIZO LA TABLA DEL HOSPITAL
        $this->kidney_model->addDonorTest($donor->id, $data); //Edita o actualiza




        //Ingresar este paciente en la base de datos de NodeJS
        $nodejsData = array(
            "id" => $donor->id,
            "patient_id" => $donor->patient_id,
            "bloodgroup" =>  $data["bloodgroup"],
            "antigenos" => [
                $test->ant1,
                $test->ant2,
                $test->ant3,
                $test->ant4,
                $test->ant5,
                $test->ant6
            ]
        );



        $this->curl->post("/api/donor", $nodejsData);
        return;
    }


    public function savePatientMedicalInfoApi()
    {
        //Solo permitir peticiones POST
        if (!$this->isPost()) return;

        //Obtener el paciente al cual se le ingresara el examen
        $patient = $this->patient_model->getPatientByPatientId($this->input->post('patient_id'));

        //Comprobar que los datos sean correctos
        $data = array(
            "bloodgroup" => $patient->bloodgroup,
            "birthdate" => $patient->birthdate,
            "sex" => $patient->sex,
            "weight" => $this->input->post('weight'),
            "ant1" =>  $this->input->post('ant1'),
            "ant2" =>  $this->input->post('ant2'),
            "ant3" =>  $this->input->post('ant3'),
            "ant4" =>  $this->input->post('ant4'),
            "ant5" =>  $this->input->post('ant5'),
            "ant6" =>  $this->input->post('ant6'),
            "patient_id" => $patient->patient_id,
        );


        //Ingresar los datos a la base de datos
        $this->kidney_model->addPatientTest($patient->patient_id, $data);

        //Ingresar este paciente en la base de datos de NodeJS
        $nodejsData = array(
            "id" => $patient->patient_id,
            "bloodgroup" =>  $data['bloodgroup'],
            "antigenos" => [
                $data['ant1'],
                $data['ant2'],
                $data['ant3'],
                $data['ant4'],
                $data['ant5'],
                $data['ant6']
            ]
        );

        $r = $this->curl->post("/api/patient", $nodejsData);

        http_response_code(201);
        header('Content-Type: application/json');
        $response = array('status' => 'added', 'response' => $r);
        echo json_encode($response);
    }

    public function saveDonorMedicalInfoApi()
    {
        //Solo permitir peticiones POST
        if (!$this->isPost()) return;

        //Obtener el paciente al cual se le ingresara el examen
        $donor = $this->kidney_model->getDonor($this->input->post('donor_id'));

        //Comprobar que los datos sean correctos
        $data = array(
            "bloodgroup" => $donor->bloodgroup,
            "birthdate" => $donor->birthdate,
            "sex" => $donor->sex,
            "weight" => $this->input->post('weight'),
            "ant1" =>  $this->input->post('ant1'),
            "ant2" =>  $this->input->post('ant2'),
            "ant3" =>  $this->input->post('ant3'),
            "ant4" =>  $this->input->post('ant4'),
            "ant5" =>  $this->input->post('ant5'),
            "ant6" =>  $this->input->post('ant6'),
            "donor_id" => $donor->id,
        );


        //Ingresar los datos a la base de datos
        $this->kidney_model->addDonorTest($donor->id, $data);





        //Ingresar este paciente en la base de datos de NodeJS
        $nodejsData = array(
            "id" => $donor->id,
            "patient_id" => $donor->patient_id,
            "bloodgroup" =>  $data['bloodgroup'],
            "antigenos" => [
                $data['ant1'],
                $data['ant2'],
                $data['ant3'],
                $data['ant4'],
                $data['ant5'],
                $data['ant6']
            ]
        );




        $r = $this->curl->post("/api/donor", $nodejsData);

        http_response_code(201);
        header('Content-Type: application/json');
        $response = array('status' => 'added', 'response' => $r);
        echo json_encode($response);
    }

    public function addDonorApi()
    {
        if (!$this->isPost()) return;


        //Obtener el paciente al cual se le agregara al donante
        $patient = $this->patient_model->getPatientByPatientId($this->input->post('patient_id'));

        //Si el paciente no existe, entonces dar error
        if (empty($patient)) {
            $this->errorResponse('invalid_patient');
            return;
        }

        $editing = false;


        //Generar id random
        $donor_id = strval(rand(10000, 9999999999)); //<-----------------------------------------------------------BUSCAR UNA MEJOR SOLUCION PARA IDs random
        //                                                                                                         QUE SEAN ALFANUMEROS Y VERIFICAR ALFANUMERICO



        //Si trae un id de la petición significa que esta editando
        if (!empty($this->input->post('id'))) {

            $editing = true;
            $donor_id = $this->input->post('id');
        }


        //Datos a agregar en la base de datos
        $data = array(
            "id" => $donor_id,
            "patient_id" => $patient->patient_id,
            "bloodgroup" => $this->input->post('bloodgroup'),
            "sex" => $this->input->post('sex'),
            'birthdate' => $this->input->post('birthdate')
        );




        //Ingresar los datos a la base de datos
        if (!$editing) $this->kidney_model->addDonor($this->input->post('patient_id'), $data);
        else {


            $edit_medical_info_data = array("bloodgroup" => $this->input->post('bloodgroup'));
            $this->kidney_model->editDonor($donor_id, $data);
            $this->editDonorMedicalInfo($donor_id, $edit_medical_info_data);
        }

        //Response
        http_response_code(201);
        header('Content-Type: application/json');
        $response = array('status' => 'added', 'patient_id' =>    $donor_id);
        echo json_encode($response);
    }


    private function getCompatibility($donor_id)
    {
        $val = $this->curl->get("/api/compatibility/" . $donor_id);
        return $val;
    }

    public function getCompatibilityApi()
    {
        $donor_id = $this->input->post('id');
        $this->getCompatibility($donor_id);
    }

    private function getCases($patient_id)
    {
        $val = $this->curl->get("/api/search-for-patient/" . $patient_id);
        return $val;
    }


    private function getCaseInfo($donor_a_id, $donor_b_id)
    {
        $val = $this->curl->get("/api/get-cases-info?donor_a_id=" . $donor_a_id . "&donor_b_id=" . $donor_b_id);
        return $val;
    }
}

/* End of file kidney.php */
    /* Location: ./application/modules/kidney/controllers/kidney.php */