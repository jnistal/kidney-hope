<!--sidebar end-->
<!--main content start-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<style>
.btn-container {
    display: flex;
    margin: 16px;
    margin-left: 22px;
    position: absolute;
    bottom: 0;
}

.btn:focus {
    outline-width: 3px !important;
    outline-style: dashed !important;
    outline-color: #077AF6 !important;

}

.btn-next {
    margin: 5px;
}

.form-container {
    position: relative;
    min-height: 600px;
}

.form-footer {
    height: 60px;
}

/* .form-group {
    display: flex;
}

.form-label {
    width: 150px;
} */
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel form-container">
            <header class="panel-heading">
                <?php
                if (!empty($patient->id))
                    echo lang('edit_patient');
                else
                    echo lang('add_new_patient');
                ?>
            </header>


            <input type="hidden" id="p_id" name="p_id" value="<?php echo $patient->patient_id ?>">

            <div class="bs-stepper " id="add-stepper">
                <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step" data-target="#patient-info">
                        <button type="button" class="step-trigger" role="tab" aria-controls="patient-info"
                            id="patient-info-trigger">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">Información del paciente</span>
                        </button>
                    </div>
                    <div class="line"></div>

                    <div class="step" data-target="#medical-info">
                        <button type="button" class="step-trigger" role="tab" aria-controls="medical-info"
                            id="medical-info-trigger">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Información médica</span>
                        </button>
                    </div>
                    <div class="line"></div>

                    <div class="step" data-target="#account-part">
                        <button type="button" class="step-trigger" role="tab" aria-controls="account-part"
                            id="account-part-trigger">
                            <span class="bs-stepper-circle">3</span>
                            <span class="bs-stepper-label">Cuenta</span>
                        </button>
                    </div>

                </div>
                <div class="bs-stepper-content">
                    <!-- Información del paciente-->
                    <div id="patient-info" class="content" role="tabpanel" aria-labelledby="patient-info-trigger">
                        <div class="form-group">
                            <label for="name" class="form-label"><?php echo lang('name'); ?></label>
                            <input type="text" class="form-control" name="name" id="name" autocomplete="new-password"
                                value='<?php
                                                                                                                                if (!empty($setval)) echo set_value('name');
                                                                                                                                if (!empty($patient->name)) echo $patient->name;
                                                                                                                                ?>'
                                placeholder="">
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="address"><?php echo lang('address'); ?></label>
                            <input type="text" class="form-control" name="address" id="address" value='<?php
                                                                                                        if (!empty($setval)) echo set_value('address');
                                                                                                        if (!empty($patient->address)) echo $patient->address;
                                                                                                        ?>'
                                autocomplete="new-password" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="phone" class="form-label"><?php echo lang('phone'); ?></label>
                            <input type="text" class="form-control" name="phone" id="phone" value='<?php
                                                                                                    if (!empty($setval)) echo set_value('phone');
                                                                                                    if (!empty($patient->phone)) echo $patient->phone;
                                                                                                    ?>'
                                autocomplete="new-password" placeholder="">
                        </div>


                        <div class="form-group">
                            <label for="birthdate" class="form-label"><?php echo lang('birth_date'); ?></label>
                            <input class="form-control form-control-inline input-medium default-date-picker"
                                id="birthdate" type="text" name="birthdate"
                                value="<?php if (!empty($setval)) echo set_value('birthdate');
                                                                                                                                                                if (!empty($patient->birthdate)) echo $patient->birthdate; ?>"
                                placeholder="" autocomplete="new-password">
                        </div>


                        <div class="form-group">
                            <label class="form-label"><?php echo lang('sex'); ?></label>
                            <select class="form-control m-bot15" name="sex" value=''>
                                <option value="Male" <?php
                                                        if (
                                                            !empty($setval) && (set_value('sex') == 'Male') ||
                                                            !empty($patient->sex) && $patient->sex == 'Male'
                                                        ) echo 'selected';
                                                        ?>> Male
                                </option>

                                <option value="Female" <?php
                                                        if (
                                                            !empty($setval) && (set_value('sex') == 'Female') ||
                                                            !empty($patient->sex) && $patient->sex == 'Female'
                                                        ) echo 'selected';
                                                        ?>> Female
                                </option>


                                <option value="Others" <?php
                                                        if (
                                                            !empty($setval) && (set_value('sex') == 'Others') ||
                                                            !empty($patient->sex) && $patient->sex == 'Others'
                                                        ) echo 'selected';
                                                        ?>> Others
                                </option>

                            </select>
                        </div>

                    </div>
                    <!-- Cuenta-->

                    <div id="medical-info" class="content" role="tabpanel" aria-labelledby="medical-info-trigger">

                        <div class="form-group">
                            <label for="doctor"><?php echo lang('doctor'); ?></label>
                            <select class="form-control m-bot15 js-example-basic-single" name="doctor" id="doctor"
                                value=''>
                                <?php foreach ($doctors as $doctor) { ?>
                                <option value="<?php echo $doctor->id; ?>"
                                    <?php if (!empty($patient->doctor) && $patient->doctor == $doctor->id) echo 'selected'; ?>>
                                    <?php echo $doctor->name; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="boodgroup"><?php echo lang('blood_group'); ?></label>
                            <select class="form-control m-bot15" name="bloodgroup" value=''>
                                <?php foreach ($groups as $group) { ?>
                                <option value="<?php echo $group->group; ?>" <?php
                                                                                    if (!empty($setval) && $group->group == set_value('bloodgroup')) echo 'selected';
                                                                                    if (!empty($patient->bloodgroup) && $group->group == $patient->bloodgroup) echo 'selected';
                                                                                    ?>>
                                    <?php echo $group->group; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>



                    </div>

                    <div id="account-part" class="content" role="tabpanel" aria-labelledby="account-part-trigger">

                        <div class="form-group">
                            <label for="image_url"><?php echo lang('image'); ?></label>
                            <input type="file" name="img_url" id="image_url">
                        </div>

                        <div class="form-group">
                            <label for="email"><?php echo lang('email'); ?></label>
                            <input type="text" class="form-control" name="email" id="email"
                                value='<?php if (!empty($patient->email)) echo $patient->email; ?>'
                                autocomplete="new-password" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="password"><?php echo lang('password'); ?></label>
                            <input type="password" class="form-control" name="password" id="password" placeholder=""
                                autocomplete="new-password">
                        </div>



                    </div>

                </div>

                <div class="form-footer"></div>

            </div>

            <div class="btn-container">
                <button class="btn btn-next" id="prev-btn">Anterior</button>
                <button class="btn btn-info btn-next" id="next-btn">Siguiente</button>
                <button class="btn btn-info btn-next" id="add-btn">Agregar Paciente</button>
                <button class="btn btn-info btn-next" id="edit-btn">Editar Paciente</button>

            </div>
        </section>
        <!-- page end-->
    </section>
</section>

<div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true" data-delay=3000
    id="toast">
    <div class="toast-header">
        Toast Header
    </div>
    <div class="toast-body">
        Some text inside the toast body
    </div>
</div>

<!--main content end-->
<!--footer start-->
<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



<!-- Helpers -->
<script>
const urlParams = new URLSearchParams(window.location.search);
const currentPatientId = urlParams.get('id');
let isEditing = false;
if (currentPatientId) isEditing = true;
</script>

<!-- Manejar botones de stepper (anterior, siguiente, añadir, editar) 
     Además maneja el autofocus de cada paso del stepper-->
<script>
$(document).ready(() => {
    //Inicializar el stepper
    let stepperEl = document.getElementById("add-stepper");
    let stepper = new Stepper(stepperEl);

    //Desactivar los botones inicialmente
    $("#prev-btn").prop("disabled", true);
    $("#add-btn").hide();
    $("#edit-btn").hide();
    $("#name").focus()

    //Id del boton de accion (agregar o editar)
    let actionBtnId = !isEditing ? "#add-btn" : "#edit-btn";

    //Activar y desactivar los botones
    stepperEl.addEventListener("show.bs-stepper", event => {
        let i = event.detail.indexStep;

        if (i == 2) $("#next-btn").hide();
        else $("#next-btn").show();

        if (i == 0) $("#prev-btn").prop("disabled", true);
        else $("#prev-btn").prop("disabled", false);;

        if (i != 2) $(actionBtnId).hide();
        else $(actionBtnId).show();

    });

    //Autofocus al cambiar de paso en el stepper
    stepperEl.addEventListener("shown.bs-stepper", event => {
        let i = event.detail.indexStep;
        if (i == 0) $("#name").focus()
        else if (i == 1) $("#doctor").focus()
        else if (i == 2) $("#image_url").focus()

    });


    //Eventos de los botones

    document.getElementById('next-btn').onclick = () => {
        stepper.next()
    };
    document.getElementById('prev-btn').onclick = () => {
        stepper.previous()
    };

    document.getElementById('add-btn').onclick = () => {
        if (!isEditing) addPatient()
    };

    document.getElementById('edit-btn').onclick = () => {
        if (isEditing) editPatient();
    };

});
</script>


<!-- Añadir y editar pacientes -->

<script>
/**
 * Obtiene los datos del formulario 
 */
function getFormData() {

    const formData = new FormData();

    formData.append("name", document.getElementsByName("name")[0].value);
    formData.append("address", document.getElementsByName("address")[0].value);
    formData.append("phone", document.getElementsByName("phone")[0].value);
    formData.append("birthdate", document.getElementsByName("birthdate")[0].value);
    formData.append("sex", document.getElementsByName("sex")[0].value);
    formData.append("doctor", document.getElementsByName("doctor")[0].value);
    formData.append("bloodgroup", document.getElementsByName("bloodgroup")[0].value);
    formData.append("email", document.getElementsByName("email")[0].value);
    formData.append("password", document.getElementsByName("password")[0].value);

    if (document.getElementsByName("img_url")[0].files.length > 0)
        formData.append("img_url", document.getElementsByName("img_url")[0].files[0]);

    if (isEditing) {
        formData.append('id', currentPatientId);
        formData.append('p_id', document.getElementsByName("p_id")[0].value);
    }
    return formData;
}




/**
 * Maneja los errores y los notifica
 */
function notifyError(errorId) {
    switch (errorId) {
        case "form_error":
            toastr.error("Datos no ingresados correctamente");
            break;
        case "this_email_address_is_already_registered":
            toastr.error("Correo electrónico ya se encuentra registrado");
            break;
        case "limit_exceeded":
            toastr.error("Limite de pacientes a sido excedido");
            break;
        default:
            toastr.error("Error");
            break;
    }
}


/**
 * Realiza una petición para añadir un nuevo paciente
 */
async function addPatient() {
    let addBtn = document.getElementById("add-btn");
    addBtn.disabled = true;


    try {
        const formData = getFormData();

        const response = await axios.post("/patient/addPatientApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        window.location.replace(`/patient/parents?id=${response.data.patient.patient_id}`);
    } catch (error) {
        console.log(error)
        addBtn.disabled = false;

        if (error.response) {

            let errorId = error.response.data.error;
            notifyError(errorId);
        }
    }
}


/**
 * Realiza una petición para editar un paciente
 */
async function editPatient() {
    let editBtn = document.getElementById("edit-btn");
    editBtn.disabled = true;

    try {
        const formData = getFormData();
        const response = await axios.post("/patient/editPatientApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        window.location.replace(`/patient`);
    } catch (error) {
        editBtn.disabled = false;
        if (error.response) {
            let errorId = error.response.data.error;
            notifyError(errorId);
        }
    }
}
</script>