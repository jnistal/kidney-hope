<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<style>
.modal-info-container {
    margin: 50px;
}

.info-lbl {
    text-transform: uppercase;
    font-size: 0.9rem;
    font-weight: 600;
}

.info-data {
    font-size: 1.5rem;
    margin: 0px;
    margin-bottom: 20px;

}

.profile-container {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.profile-picture {
    border-radius: 20px;
    width: 200px;
    height: 200px;
    object-fit: cover;
}

.title {
    text-align: center;
    font-size: 2rem;
    margin: 0px;
}

.subtitle {
    text-align: center;
    font-size: 1.3rem;
    margin: 0px;
    margin-top: 5px;
}

.info-container {
    margin-top: 10px;
}

.container {
    background-color: red;
    display: flex;
}

.parents-container {
    background-color: blue;
}

.btn-container {
    display: flex;
    margin-bottom: 10px;
}

.patient-btn {
    display: flex;
    flex-direction: column;
    justify-content: center;
}
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <br />
        <div class="row">
            <div class="profile-container col-md-3">
                <img src="<?php if (!empty($patient->img_url)) echo $patient->img_url;
                            else echo '/hospital/common/img/blank-profile.jpg'; ?>" class="profile-picture">
                <div class="info-container">
                    <h3 class="title"><?php echo $patient->name; ?> </h3>
                    <h3 class="subtitle"><?php echo $patient->email; ?> </h3>
                    <br />
                    <div class="patient-btn">
                        <button class="btn btn-info" id="view-patient">Ver</button>
                        <button class="btn btn-warning" id="edit-patient">Editar</button>
                    </div>

                </div>

            </div>

            <div class="col-md-9">
                <div class="btn-container">
                    <div style="flex-grow:1;"></div>
                    <button class="btn btn-primary" id="add-btn">Agregar Representante</button>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($parents as $parent) : ?>
                        <tr>
                            <th scope="row"><?php echo $parent->id; ?></th>
                            <td><?php echo $parent->name; ?></td>
                            <td><?php echo $parent->phone; ?></td>
                            <td>

                                <button class="btn btn-info" id="<?php echo 'info-' . $parent->id; ?>"
                                    onClick='openViewParent(<?php echo json_encode($parent); ?>)'>Ver</button>
                                <button class="btn btn-warning" id="<?php echo 'edit-' . $parent->id; ?>"
                                    onClick='openEditParent(<?php echo json_encode($parent); ?>)'>Editar</button>
                                <button class="btn btn-danger"
                                    onclick='openRemoveParent(<?php echo json_encode($parent); ?>)'>Eliminar</button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="largeModal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="add-modal-title">Agregar representante</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="form-label"><?php echo lang('name'); ?></label>
                            <input type="text" class="form-control" name="name" id="name" autocomplete="new-password"
                                placeholder="">
                        </div>


                        <div class="form-group">
                            <label for="email" class="form-label"><?php echo lang('email'); ?></label>
                            <input type="text" class="form-control" name="email" id="email" autocomplete="new-password"
                                placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="form-label"><?php echo lang('phone'); ?></label>
                            <input type="text" class="form-control" name="phone" id="phone" autocomplete="new-password"
                                placeholder="">
                        </div>


                        <div class="form-group">
                            <label for="dpi" class="form-label">DPI</label>
                            <input type="text" class="form-control" name="dpi" id="dpi" autocomplete="new-password"
                                placeholder="">
                        </div>

                        <div class="form-group">
                            <label class="form-label"><?php echo lang('sex'); ?></label>
                            <select class="form-control m-bot15" name="sex" id="sex" value=''>
                                <option value="Male" selected> Male</option>
                                <option value="Female"> Female </option>
                                <option value="Others"> Others </option>

                            </select>

                        </div>

                        <div class="form-group">
                            <label for="address" class="form-label"><?php echo lang('address'); ?></label>
                            <input type="text" class="form-control" name="address" id="address"
                                autocomplete="new-password" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="birthdate" class="form-label"><?php echo lang('birth_date'); ?></label>
                            <input type="text" class="form-control form-control-inline input-medium default-date-picker"
                                name="birthdate" id="birthdate" autocomplete="new-password" placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="cancel-btn-1">Cancelar</button>
                        <button type="button" id="save-btn" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="largeModal"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <br />

                        <h4 id="confirmationText">¿Estas seguro de eliminar este representante
                            ?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn" id="cancel-btn-2">Cancelar</button>
                        <button type="button" class="btn btn-danger" id="delete-btn">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>




        <div class="modal fade" id="patient-modal" tabindex="-1" role="dialog" aria-labelledby="largeModal"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <br />

                        <h4 id="confirmationText">Paciente</h4>
                    </div>
                    <div class="modal-content">
                        <div class="modal-info-container">
                            <small class="info-lbl">Nombre</small>
                            <h4 class="info-data"><?php echo $patient->name; ?></h4>
                            <small class="info-lbl">Email</small>
                            <h4 class="info-data"><?php echo $patient->email; ?></h4>
                            <small class="info-lbl">Dirección</small>
                            <h4 class="info-data"><?php echo $patient->address; ?></h4>
                            <small class="info-lbl">Telefono</small>
                            <h4 class="info-data"><?php echo $patient->phone; ?></h4>
                            <small class="info-lbl">Sexo</small>
                            <h4 class="info-data"><?php echo $patient->sex; ?></h4>
                            <small class="info-lbl">Fecha de nacimiento</small>
                            <h4 class="info-data"><?php echo $patient->birthdate; ?></h4>
                            <small class="info-lbl">Doctor</small>
                            <h4 class="info-data"><?php echo $doctor->name; ?></h4>
                            <small class="info-lbl">Tipo de sangre</small>
                            <h4 class="info-data"><?php echo $patient->bloodgroup; ?></h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn" id="cancel-btn-2">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>


<script src="common/js/jquery.js"></script>
<script src="common/js/jquery-1.8.3.min.js"></script>
<script src="common/js/bootstrap.min.js"></script>
<script src="common/js/bs-stepper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>
const urlParams = new URLSearchParams(window.location.search);
const currentPatientId = urlParams.get('id');


let title = document.getElementById('add-modal-title');
let name = document.getElementById('name');
let email = document.getElementById('email');
let phone = document.getElementById('phone');
let dpi = document.getElementById('dpi');
let sex = document.getElementById('sex');
let address = document.getElementById('address');
let birhdate = document.getElementById('birhdate');
let saveBtn = document.getElementById('save-btn');
let addBtn = document.getElementById('add-btn');
let deleteBtn = document.getElementById('delete-btn');
let cancelBtn1 = document.getElementById("cancel-btn-1");
let cancelBtn2 = document.getElementById("cancel-btn-2");

let editPatientBtn = document.getElementById("edit-patient");
let viewPatientBtn = document.getElementById("view-patient");

$(document).ready(() => {

    $('#add-modal').on('shown.bs.modal', e => {
        name.focus();
    });


    viewPatientBtn.onclick = () => $('#patient-modal').modal('show');
    editPatientBtn.onclick = () => $(location).attr('href', `patient/editPatient?id=${currentPatientId}`);
    addBtn.onclick = () => openAddParent();
});



/**
 * Abre el modal de abrir representantes
 */
function openAddParent() {
    //Remover valores anteriores
    title.innerHTML = "Agregar Representante";
    name.value = email.value = phone.value = dpi.value = address.value = birthdate.value = '';
    sex.value = 'Male';
    setFormEnable(false);
    $('#add-modal').modal('show');
    saveBtn.onclick = () => addParent();
}


/**
 * Abre el modal de ver información del representante
 */
function openViewParent(parent) {
    openEditParent(parent);
    setFormEnable(true);

}



function getFormData() {

    const formData = new FormData();

    formData.append("name", name.value);
    formData.append("address", address.value);
    formData.append("phone", phone.value);
    formData.append("birthdate", birthdate.value);
    formData.append("sex", sex.value);
    formData.append("email", email.value);
    formData.append("dpi", dpi.value);
    formData.append("id", currentPatientId);

    // if (isEditing) {
    //     formData.append('id', currentPatientId);
    //     formData.append('p_id', document.getElementsByName("p_id")[0].value);
    // }
    return formData;
}

function notifyError(errorId) {
    switch (errorId) {
        case "form_error":
            toastr.error("Datos no ingresados correctamente");
            break;
        case "this_email_address_is_already_registered":
            toastr.error("Correo electrónico ya se encuentra registrado");
            break;
        case "limit_exceeded":
            toastr.error("Limite de pacientes a sido excedido");
            break;
        default:
            toastr.error("Error desconocido");
            break;
    }
}

async function addParent(parent) {
    cancelBtn1.disabled = saveBtn.disabled = true;

    try {
        const formData = getFormData();
        const response = await axios.post("/hospital/patient/addPatientParentsApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        window.location.reload(false);
    } catch (error) {
        saveBtn.disabled = cancelBtn1.disabled = false;

        if (error.response) {
            let errorId = error.response.data.error;
            notifyError(errorId);
        } else notifyError("default");
    }

}



async function editParent(parent) {
    cancelBtn1.disabled = saveBtn.disabled = true;

    try {
        const formData = getFormData();
        formData.append("parent_id", parent.id);
        const response = await axios.post("/hospital/patient/editPatientParentsApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        window.location.reload(false);
    } catch (error) {
        saveBtn.disabled = cancelBtn1.disabled = false;

        if (error.response) {
            let errorId = error.response.data.error;
            notifyError(errorId);
        } else notifyError("default");
    }
}



/**
 * Abre el modal de editar representante
 */
function openEditParent(parent) {
    title.innerHTML = "Editar Representante";
    name.value = parent.name;
    email.value = parent.email;
    phone.value = parent.phone;
    dpi.value = parent.dpi;
    address.value = parent.address;
    birthdate.value = parent.birthdate;
    sex.value = parent.sex;
    saveBtn.onclick = () => editParent(parent);
    setFormEnable(false);
    $('#add-modal').modal('show');
}

function setFormEnable(value) {
    name.disabled =
        email.disabled =
        phone.disabled =
        dpi.disabled =
        address.disabled =
        birthdate.disabled =
        sex.disabled =
        value;

}

/**
 * Abre el dialogo de confirmacion para eliminar representante
 */
function openRemoveParent(parent) {

    $('#confirm').modal('show');
    deleteBtn.onclick = () => {
        deleteParent(parent)
    }
}



async function deleteParent(parent) {
    cancelBtn2.disabled = deleteBtn.disabled = true;

    try {

        const formData = new FormData();
        formData.append("parent_id", parent.id);

        const response = await axios.post("/hospital/patient/deletePatientParentsApi", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        window.location.reload(false);
    } catch (error) {
        deleteBtn.disabled = cancelBtn2.disabled = false;

        if (error.response) {
            let errorId = error.response.data.error;
            notifyError(errorId);
        } else notifyError("default");
    }
}
</script>