/**
 * @param {Any} patient Información del paciente
 * Obtiene los datos del formulario
 */
function getFormData(donor) {
  const formData = new FormData();
  formData.append("ant1", document.getElementById("ant1Input").value);
  formData.append("ant2", document.getElementById("ant2Input").value);
  formData.append("ant3", document.getElementById("ant3Input").value);
  formData.append("ant4", document.getElementById("ant4Input").value);
  formData.append("ant5", document.getElementById("ant5Input").value);
  formData.append("ant6", document.getElementById("ant6Input").value);
  formData.append("weight", document.getElementById("weightInput").value);
  formData.append("donor_id", donor.id);
  return formData;
}

/**
 *
 * @param {Any} patient Información del paciente
 */

async function addPatientExam(patient) {
  let data = getFormData(patient);
  let saveBtn = document.getElementById("saveBtn");
  saveBtn.disabled = true;
  try {
    const response = await axios.post("/kidney/saveDonorMedicalInfoApi", data, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    saveBtn.disabled = false;

    window.location.reload();
  } catch (error) {
    toastr.error("Error desconocido");
    saveBtn.disabled = false;
  }
}
